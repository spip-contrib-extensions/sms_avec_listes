<?php

/**

 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Renvoie les contacts extrait d'un objet SPIP prêt à une insertion dans une liste du prestataire
 *
 * @param array $arg
 *        Tableau donnant le schéma d'extraction du ou des objets 
 *                - `schemas`, nom des schémas d'importation
 *                - `from`, table de l'objet auxquels les numéros sont associés et autres tables a interroger
 *                - `where`, facultatif, conditions supplémentaires
 *                - `select`, facultatif, nom des colonnes de la table dont ont souhaite que les informations accompagnes le contact
 *                          (un nom, une date, un lieu concernant le contact)
 *                - `force`, facultatif, laisse les numéros de téléphone en doublons
 *        Un exemple du tableau est dans le fichier ../sms_avec_listes/sms_listes/auteurs.yaml
 * @return array
 *        Tableau des contacts
 */
function sms_listes_export_objets_dist($arg = []) {
	$data = [];
	// Le schéma d'extraction peut proposer l'export de plusieurs objets en même temps.
	if (isset($arg['schemas'])){
		include_spip('inc/config');
		foreach ($arg['schemas'] as $key => $_schema) {
			// la première table indique l'objet auxquels les numéros sont associés
			$table = $arg['from'][$_schema][0];
			// cette table doit être déclarée comme pouvant accueillir des coordonnées
			if (!in_array($table, lire_config('coordonnees/objets',[]))){
				return false;
			}
			include_spip('base/objets');
			$objet = objet_type($table);
			// Recherche des numéros de téléphones portables associés à l'objet
			// -- l'objet est dans une table de liens qui contient l'identifiant unique du numéro et aussi son type.
			//    Le type pour un téléphone portable est ’cell’.
			// -- le numéros de téléphone est dans la table spip_numeros auquel on accède par l'identifiant unique,
			// Il est donc nécessaire de faire une jointure des deux tables avec l'identifiant unique.
			$select = ['numeros.numero as value'];
			$from = ['spip_numeros_liens as liens', 'spip_numeros as numeros'];
			$where = [
				'liens.objet=' . sql_quote($objet),
				'liens.id_numero=numeros.id_numero',
				'liens.type=' . sql_quote('cell'),
			];
			// permettre la liaison avec des tables supplémentaires
			if (isset($arg['from'][$_schema]) and $arg['from'][$_schema] and is_array($arg['from'][$_schema])){
				foreach ($arg['from'][$_schema] as $key => $value) {
					$from[] = $value;
				}
			}
			// permettre des conditions supplémentaires
			if (isset($arg['where'][$_schema]) and $arg['where'][$_schema] and is_array($arg['where'][$_schema])){
				foreach ($arg['where'][$_schema] as $key => $value) {
					$where[] = $value;
				}
			}
			// permettre des selections supplémentaires (qui deviendront les infos)
			if (isset($arg['select'][$_schema]) and $arg['select'][$_schema] and is_array($arg['select'][$_schema])){
				foreach ($arg['select'][$_schema] as $key => $value) {
					// certaines infos peuvent être laissées vides
					if ($value and in_array($key, ['info1', 'info2', 'info3', 'info4'])) {
						$select[] = $value . " AS $key";
					}
				}
			}
			if ($retour = sql_allfetsel($select, $from, $where)){
				// permettre la substitution post_requete
				if (isset($arg['post_requete'][$_schema]) and $arg['post_requete'][$_schema] and is_array($arg['post_requete'][$_schema])){
					$retour = substituer_infos($retour, $arg['post_requete'][$_schema]);
				}
				$data = array_merge($data,$retour);
			}
		}
		// normaliser les numéros et retirer les contacts non conforme
		// retirer les doublons
		if (!isset($arg['force'])){
			$data = numero_conforme($data, 'value');
			$data = unique_multidim_array($data, 'value');
		}
	}
	return $data;
}

/**
 * Fonction privée palliant au fait que array_unique() ne fonctionne pas avec des tableaux multidimensionnels.
 *
 * Si les numéros de téléphone ne sont pas normalisés, la fonction ne pourra pas détecter tous les doublons
 * (par exemple 33 6 07 08 09 10 sera considéré comme différent de 06 07 08 09 10)
 * C'est une des raisons de la possibilité de procéder à une normalisation des numéros 
 * avec le menu Maintenance|Maintenance des listes.
 *
 * Crédit : Ghanshyam Katriya
 */

function unique_multidim_array($array, $key) { 
	$temp_array = array(); 
	$i = 0; 
	$key_array = array(); 
	foreach($array as $val) {
		if (!in_array($val[$key], $key_array)) { 
			$key_array[$i] = $val[$key]; 
			$temp_array[$i] = $val; 
		} else {
			// valeur en doubon
			spip_log("doublon du numéro " . $val[$key] . ' suppression du contact : ' . print_r($val,true), 'sms_liste.' . _LOG_INFO_IMPORTANTE);
		}
		$i++; 
	} 
	return $temp_array; 
} 

function numero_conforme($array, $key){
	$options_enventuelles['normaliser'] = 'oui';
	$options_enventuelles['pays'] = 'fr';
	$options_enventuelles['normalisation_prefix'] = '33';
	$options_enventuelles['seulement'] = 'portable';
	$type_de_test = 'numero_e164';
	$verifier = charger_fonction('verifier', 'inc/');
	foreach($array as $index => $val) {
		if ($val[$key]) {
			spip_log('$val[$key]='.$val[$key], 'sms_liste.' . _LOG_INFO_IMPORTANTE);
			// il faut vérifier la validité des numéros transmis
			if ($erreur = $verifier($val[$key], $type_de_test, $options_enventuelles, $normalise)){
				// le contact n'a pas un format reconnu
				// on supprime le contact
				spip_log('Numéro non conforme ' . $val[$key] . ' suppression du contact : ' . print_r($val,true), 'sms_liste.' . _LOG_INFO_IMPORTANTE);
				unset($array[$index]);
			}
			$array[$index][$key] = $normalise;
		} else {
			// le contact n'a pas de téléphone
			// on supprime le contact
			spip_log('Numéro non conforme ' . $val[$key] . ' suppression du contact : ' . print_r($val,true), 'sms_liste.' . _LOG_INFO_IMPORTANTE);
			unset($array[$index]);
		}
	}
	return $array;
}

function substituer_infos($array, $substituer) {
	// avoir un tableau des clés de substituer
	$infos = array_keys($substituer);
	foreach($array as $cle => $valeurs) {
		foreach($infos as $_info){
			// il y a une valeur que l'on peut substituer
			if (isset($valeurs[$_info]) and isset($substituer[$_info][$valeurs[$_info]])) { 
				// si la valeur est une chaine de langue, on la traite
				if (preg_match("/^(&lt;:|<:)/", $substituer[$_info][$valeurs[$_info]])) {
					$substituer[$_info][$valeurs[$_info]] = preg_replace("/^(&lt;:|<:)/", "", $substituer[$_info][$valeurs[$_info]]);
					$substituer[$_info][$valeurs[$_info]] = preg_replace("/(:&gt;|:>)$/", "", $substituer[$_info][$valeurs[$_info]]);
					$substituer[$_info][$valeurs[$_info]] = _T($substituer[$_info][$valeurs[$_info]]);
				}
				$array[$cle][$_info] = $substituer[$_info][$valeurs[$_info]];
			}
		}
	}
	return $array; 
} 