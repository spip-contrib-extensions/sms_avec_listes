<?php

/**
 * Fichier avec la fonction surchargeable renvoyant les objets 
 * ayant ou pouvant avoir un numéro de téléphone portable associé.
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Renvoie un tableau des objets ayant ou pouvant avoir un numéro de téléphone portable associé
 *
 * @param array $arg
 *        tableau - `exclus`, facultatif, sous tableau des tables dont on ne souhaite pas exporter les numéros
 *                          ```$data = $objets_portables(['exclus' => ['spip_auteurs']]);```
 *                  `objets`, facultatif, sous tableau des tables que l'on souhaite exporter
 *                          ```$data = $objets_portables(['objets' => ['spip_auteurs']]);```
 *                  `force`, faculatif, permet de forcer à l'établissement d'une liste d'objets 
 *                           incluant les objets stérils (sans association avec un téléphone portable)
 *                          ```$data = $objets_portables(['force' => 'oui']);```
 *        en l'absence d'argument, toutes les tables ayant un numéro associé à son objet seront retournée 
 * @return array
 *        Tableau 
 *           nom de la table => nom de l'objets et nombre de contacts ayant un téléphone portable
 */
function sms_listes_objets_portables_dist($arg = []) {
	include_spip('inc/config');
	$data = [];
	// Examiner les objets soit
	// -- à partir de la configuration du plugin Coordonnées (choisir_objets)
	// -- à partir de la liste transmise le cas échéant
	// après
	// o  avoir exclus les tables signalées le cas échéant
	// o  avoir exclus les tables sans association (sauf si forcé)
	// et renvoyer un tableau indiquant la table de l'objet, l'objet et le nombre de téléphone portable associé.
	if ( isset($arg['objets']) or $config = lire_config('coordonnees/objets',[]) ){
		if ( isset($arg['objets']) ){
			$config = $arg['objets'];
		}
		// exclusion des tables
		if ( isset($arg['exclus']) and is_array($arg['exclus']) ){
			foreach ($arg['exclus'] as $value) {
				$cle = array_search($value, $config);
				unset($config[$cle]);
			}
		}
		if (isset($config)){
			foreach ($config as $table) {
				if (!empty($table)){ // des valeurs vides peuvent s'être glissées dans la configuration
					$objet = objet_type($table);
					// Compter les numeros de téléphones portables qui sont associés à l'objet
					// -- l'objet est dans une table de liens qui contient l'identifiant unique du numéro et aussi son type.
					//    Le type pour un téléphone portable est ’cell’.
					// -- le numéros de téléphone est dans la tables spip_numeros auquel on accède par l'identifiant unique,
					// Il est donc nécessaire de faire une jointure des deux table avec l'identifiant unique.
					$from = ['spip_numeros_liens as liens', 'spip_numeros as numeros'];
					$where = [
						'liens.objet=' . sql_quote($objet),
						'liens.id_numero=numeros.id_numero',
						'liens.type=' . sql_quote('cell'),
					];
					// on ne retourne que des objets qui ont effectivement des téléphones portables associés
					$count = sql_countsel($from, $where);
					if (isset($arg['force'])){
						$data[$table] = "$objet ($count)";
					} elseif ($count) {
						$data[$table] = "$objet ($count)";
					}
				}
			}
		}
	}
	return $data;
}
