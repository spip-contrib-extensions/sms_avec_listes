<?php
/**
 * Fichier gérant l'installation et désinstallation du plugin SMS_avec_listes
 *
 * @plugin     sms_avec_listes
 * @copyright  2022
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\Sms_avec_listes\Administrations
 */


if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Fonction d'installation et de mise à jour du plugin
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @param string $version_cible
 *     Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 * @return void
 */
function sms_avec_listes_upgrade($nom_meta_base_version, $version_cible) {
	$maj = array();

	$maj['create'] = [
		['ecrire_config','sms_avec_listes/heures_sans_sms', ['20:00','08:00']],
		['ecrire_config','sms_avec_listes/jours_sans_sms', [['0','6']]],
		['ecrire_config','sms_avec_listes/envoi', 'immediat'],
		['ecrire_config','sms_avec_listes/obsolescence', '30'],
	];
	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}


/**
 * Fonction de désinstallation du plugin
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @return void
 */
function sms_avec_listes_vider_tables($nom_meta_base_version) {
	effacer_config("sms_avec_listes");
	effacer_meta($nom_meta_base_version);
}
