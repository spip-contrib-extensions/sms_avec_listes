<?php

/**
*/

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// retourne les crédits restants

function sms_credits($token){
	$instruction = 'credits';
	if ( $lister_sms = charger_fonction('lister_sms', 'inc') and $liste = $lister_sms($instruction)){
		if ( $liste['message'] == 'OK' and isset($liste['credits']) and $credits = (int) $liste['credits'] ){
			return $credits;
		}
	}
	return '';
}

function sms_tz(){
	return date_default_timezone_get();
}