<?php
/**
 * Fonctions du squelette associé
 *
 * @package SPIP\Squelettes\contenu\Sms_contact\UI
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Fonction utilisée en filtre pour obtenir les informations sur un contact d'une liste
 *
 * smsfactor
 *      L'API du prestataire n'a pas de commande pour lire directement les informations d'un contact.
 *      Il faut lister les contacts de la liste et retrouver le contact.
 *
 * @uses lister_sms()
 *
 * @param string $idcontact
 * @param int $id_liste 
 *
 * @return array|void
 *      Tableau des informations relatives à la liste, dans la clé éponyme, et au contact, dans la clé éponyme.
 *      Vide si erreur.
 */
function sms_infos_contacts($idcontact, $id_liste){
	$retours = [];
	// on vérifie la validité de l'identifiant de l'id_liste transmis
	if ($id = (int) $id_liste and $idcontact = (string) $idcontact and strlen($idcontact) and !in_array($idcontact,['oui','new'])) {
		if ($lister_sms = charger_fonction('lister_sms', 'inc')
			and $retour = $lister_sms('annuaire')
			and $retour['message'] == 'OK'
			and $ids = array_column($retour['lists'],'id')
			and in_array($id_liste, $ids)
		) {
			// la requete nous a permis de vérifier la validité de l'identifiant transmis
			// on transmet les infos de la liste
			$cle = array_search($id_liste, $ids);
			$retours['liste'] = $retour['lists'][$cle];
			
		} else {
			// l'identifiant de la liste n'a pas été trouvé dans l'annuaire (soit il n'y est pas, soit la requete a échoué).
			return '';
		}
		// on charge les valeurs de l'idcontact
		if ($retour = $lister_sms('lister',[$id_liste])
			and $retour['message'] == 'OK'
		) {
			foreach ($retour['list'] as $key => $value) {
				if ($value['id'] === $idcontact) {
					$retours['contact']['idcontact'] = $idcontact;
					$retours['contact']['destination'] = $value['destination'];
					$retours['contact']['info1'] = $value['info1'];
					$retours['contact']['info2'] = $value['info2'];
					$retours['contact']['info3'] = $value['info3'];
					$retours['contact']['info4'] = $value['info4'];
					return $retours;
				}
			}
			if (!isset($valeurs['destination'])){
				// l'indentifiant du contact n'a pas été trouvé dans la liste (soit il n'y est pas, soit la requete a échoué).
				return '';
			}
		}
	} else {
		// il n'y a pas les identifiants nécessaires pour avoir les informations sur le contact.
		return '';
	}
	return '';
}