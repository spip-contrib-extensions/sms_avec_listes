<?php

/**
*/

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// retourne la liste des objets ayant un numéro de téléphone portable associé ou associable
function sms_lister_objets($arg=[]){
	$objets_portables = charger_fonction('objets_portables', 'sms_listes');
	return $objets_portables($arg);
}