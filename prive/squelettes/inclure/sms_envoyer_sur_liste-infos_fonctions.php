<?php
/**
 * Fonctions du squelette associé
 *
 * @package SPIP\Sms_avec_listes\UI
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Fonction utilisée en filtre pour obtenir un contact de la liste
 *
 * @uses lister_sms()
 *
 * @return array|void
 *      tableau d'informations sur le contact
 */
 function sms_liste_contact($id,$n_contact){
	 if ($nbr = (int) $n_contact and $lister_sms = charger_fonction('lister_sms', 'inc')){
		$instruction = 'lister';
		$liste = $lister_sms($instruction,[$id]);
		if ( $liste['message'] == 'OK' and isset($liste['list'][$nbr - 1]) and isset($liste['contacts']) ){
			$retour = $liste['list'][$nbr - 1];
			$retour = array_merge($retour, ['contacts' => $liste['contacts']]);
			return $retour;
		}
	}
	return '';
}