<?php
/**
 * Fonctions du squelette associé
 *
 * @package SPIP\Sms_avec_listes\UI
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Fonction utilisée en filtre pour obtenir la liste des contacts de la liste
 *
 * @uses lister_sms()
 *
 * @return array|void
 *      tableau des listes
 */
function sms_liste_contacts($id){
	if ($lister_sms = charger_fonction('lister_sms', 'inc')){
		$instruction = 'lister';
		$retour = $lister_sms($instruction,[$id]);
		if ($retour['message'] == 'OK'){
			return $retour['list'];
		}
	}
	return '';
}