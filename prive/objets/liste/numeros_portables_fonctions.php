<?php

/**

 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Fonction pour avoir des statistiques sur la qualité des listes
 *
 * Combien de numéros sont normalisables 
 * Combien de numéros sont anormaux
 * Combien de numéros sont ok
 * ```[(#SET{stats,#OBJET|sms_liste_stats})] [(#GET{stats}|print)]```
 *
 * @uses API verifier
 * @param null|int $objet
 *     objet éventuel pour limiter la normalisation des numéros à ceux qui lui sont associés
 *     En absence de objet utilise l'argument de l'action sécurisée.
**/
function sms_liste_stats($objet=null) {
	include_spip('base/objets');
	if ($objet){
		$objets = [table_objet_sql($objet)];
	}
	if (!$objet){
		$objets_portables = charger_fonction('objets_portables', 'sms_listes');
		$objets =  $objets_portables([]);
		$objets = array_keys($objets);
	}

	$stats_anomalies = 0;
	$stats_normalisable = 0;
	$stats_ok = 0;
	foreach ($objets as $key => $table) {
		$objet = objet_type($table);
		// Connaitre les numeros de téléphones portables qui sont associés à l'objet
		// -- l'objet est dans une table de liens qui contient l'identifiant unique du numéro et aussi son type.
		//    Le type pour un téléphone portable est ’cell’.
		// -- le numéros de téléphone est dans la tables spip_numeros auquel on accède par l'identifiant unique,
		// Il est donc nécessaire de faire une jointure des deux table avec l'identifiant unique.
		$select = ['numeros.numero','numeros.id_numero'];
		$from = ['spip_numeros_liens as liens', 'spip_numeros as numeros'];
		$where = [
			'liens.objet=' . sql_quote($objet),
			'liens.id_numero=numeros.id_numero',
			'liens.type=' . sql_quote('cell'),
		];
		if ($liens = sql_allfetsel($select, $from, $where)) {
			foreach ($liens as $l) {
				// il faut vérifier la validité et normaliser
				$verifier = charger_fonction('verifier', 'inc/');
				$type_de_test = 'numero_e164';
				$options_enventuelles = ['pays' => 'fr','normaliser' => 'oui'];
				$numero_normalise = '';
				if ($erreur = $verifier($l['numero'], $type_de_test, $options_enventuelles, $numero_normalise)){
					// permettre de retrouver dans le log les numéros que ne peuvent être normalisés
					spip_log('sms_liste_stats ' . $erreur . ' id_numero=' . $l['id_numero'] . ' ('. $l['numero'] .')', 'sms_liste.' . _LOG_DEBUG);
					$stats_anomalies++;
				}
				// s'il y a eu normalisation, il faut faire la modification
				if ($numero_normalise and $l['numero'] != $numero_normalise){
					$stats_normalisable++;
				}
				if ($l['numero'] == $numero_normalise) {
					$stats_ok++;
				}
			}
		}
	}
	return ['anomalie' => $stats_anomalies, 'normalisable' => $stats_normalisable, 'ok' => $stats_ok];
}
