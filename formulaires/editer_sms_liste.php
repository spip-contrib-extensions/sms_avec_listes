<?php
/**
 * Gestion du formulaire d'édition d'une liste
 *
 * @plugin sms_avec_liste
 * @license 2015
 * @author tofulm, Vincent CALLIES
 * 
 * @package SPIP\Sms_avec_liste\Formulaires
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Saisies d'une liste
 *
 * @param int|string $id_liste
 *     Identifiant de la liste. 'new' pour une nouvelle liste.
 * @param string $redirect
 *     URL de redirection après le traitement
 * @return array
 *     Environnement du formulaire
 */
function formulaires_editer_sms_liste_saisies_dist($id_liste = 'new', $redirect = '', $options = []) {

	$saisies[] = [
		'saisie' => 'input',
		'options' => [
			'nom' => 'name',
			'label' => '<:sms_liste:info_name:>',
			'explication' => '<:sms_liste:explication_name:>',
		],
	];

	return $saisies;
}

/**
 * Chargement du formulaire d'édition d'une liste
 *
 * Le squelette ../sms_avec_listes/prive/squelettes/contenu/sms_liste_edit.html
 * transmets les valeurs d'environnement à la fonction.
 *
 * @param int|string $id_liste
 *     Identifiant la liste. 'new' pour une nouvelle liste.
 * @param string $redirect
 *     URL de redirection après le traitement
 * @return array
 *     Tableau des éventuelles erreurs
 */
function formulaires_editer_sms_liste_charger_dist($id_liste = 'new', $redirect = '', $options = []) {

	$valeurs = [];
	// Pour une modification, on vérifie la validité de l'identifiant transmis
	if ( $id_liste
		and !in_array($id_liste,['new', 'oui'])
		and $lister_sms = charger_fonction('lister_sms', 'inc')
		and $retour = $lister_sms('annuaire')
		and $retour['message'] == 'OK'
		and $ids = array_column($retour['lists'],'id')
		and in_array($id_liste, $ids)
	) {
		// la requete nous a permis de vérifier la validité de l'identifiant transmis
		$valeurs['id_liste'] = $id_liste;
		// mais pour l'instant la modification du nom d'une liste n'est pas encore prévue
		$valeurs['message_erreur'] = 'Il n’est pas encore possible de modifier des listes.';
		$valeurs['editable'] = false;
		
	} elseif (!in_array($id_liste,['new','oui'])) {
		// l'indentifiant n'a pas été trouvé dans l'annuaire (soit il n'y est pas, soit la requete a échoué).
		$valeurs['message_erreur'] = _T('sms_liste:err_annuaire_identifiant_existant',['id'=>$id_liste]);
		$valeurs['editable'] = false;
	}

	return $valeurs;
}

/**
 * Traitements du formulaire d'édition d'une liste
 * 
 * @param int|string $id_liste
 *     Identifiant la liste. 'new' pour une nouvelle liste.
 * @param string $redirect
 *     URL de redirection après le traitement
 * @return array
 *     Retour des traitements
 */
function formulaires_editer_sms_liste_traiter_dist($id_liste = 'new', $redirect = '', $options = []) {

	$name = _request('name');
	$contacts = [];

	// création d'une liste
	if (!intval($id_liste)){
		$lister_sms = charger_fonction('lister_sms', 'inc');
		$instruction = 'creer';
		$options = [
			'name' => $name,
			'contacts' => $contacts,
			'verifier' => [],
		];
		// appel de la fonction principale pour interagir avec les listes via l'API du prestataire
		$_retours = $lister_sms($instruction, [], $options);
		// analyse du resultat de la requête
		if ($_retours['message'] == 'OK'){
			// la création aboutie à un id qu'il faut indiquer
			$retours['message_ok'] = _T('sms_liste:ok_id_liste', ['id' => $_retours['id']]);
			if ($_retours['contacts'] == 0){
				$retours['message_ok'] .= ' ' .  _T('sms_liste:info_aucun_contact') . '.';
			} elseif ($_retours['contacts'] == 1){
				$retours['message_ok'] .= ' ' .  _T('sms_liste:info_1_contact') . '.';
			} elseif ($_retours['contacts'] > 1){
				$retours['message_ok'] .= ' ' .  _T('sms_liste:info_nb_contacts', ['nb' => $_retours['contacts']]) . '.';
			}
		} else {
			// il y a une erreur qu'il faut expliciter 
			$retours['message_erreur'] = $_retours['message'];
			// et rendre précise dans le log en transmettant les options utilisées si on est en DEBUG
			spip_log($_retours['message'] . print_r($options,true), 'sms_liste.' . _LOG_DEBUG);
			$retours['editable'] = false;
		}
	// modification d'une liste
	} else {
		$retours['editable'] = true;
		$retours['message_erreur'] = 'Une liste semble ne pouvoir être que créée ou supprimée sur l’API du prestataire.';
	}

	return $retours;
}