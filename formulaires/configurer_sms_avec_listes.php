<?php
if (!defined('_ECRIRE_INC_VERSION')) return;
 
 
/**
 * Un simple formulaire de config.
 *
 * La configuration permet :
 * - le choix de la modalité d'envoi du SMS (immédiatement ou en tache CRON)
 *   si l'envoi en tâche est selectionné, la temporalité sera demandée.
 * - de définir une plage horaire pour l'envoi des SMS
 *   l'utilisation de #AUTORISER{'envoyer','_sms'} y répond
 *
 * On a juste à déclarer les saisies grâce au plugin Saisies pour formulaires
 * @link https://contrib.spip.net/Formulaire-de-configuration-avec-le-plugin-Saisies
 * @link https://contrib.spip.net/Generation-de-saisies-conditionnelles-avec-afficher_si
 *
 **/
function formulaires_configurer_sms_avec_listes_saisies_dist(){
	// la temporisation est définie par une constante dans les options
	// il faut la transformer en un tableau data pour une selection
	$data = [];
	foreach (_DELAIS_DU_JOB_ENVOYER_SMS as $key => $value) {
		$data[$value] = $value;
	}

	// $saisies est un tableau décrivant les saisies à afficher dans le formulaire de configuration
	$saisies = [
		[
			'saisie' => 'radio', 
			'options' => [
				'nom' => 'envoi',
				'label' => 'Envoi du SMS',
				'data' => [
					'job' => '<:sms_liste:configuration_envoi_job:>',
					'immediat' => '<:sms_liste:configuration_envoi_immediat:>',
				],
				'explication' => '<:sms_liste:configuration_envoi_job_explication:>',
			]
		],
		[
			'saisie' => 'selection',
			'options' => [
				'nom' => 'temporisation',
				'afficher_si' => '@envoi@ == "job"',
				'label' => '<:sms_liste:configuration_temporisation:>',
				'data' => $data,
				'explication' => '<:sms_liste:configuration_temporisation_explication:>',
				'cacher_option_intro' => 'oui',
			]
		],
		[
			'saisie' => 'heures_a_heures',
			'options' => [
				'nom' => 'heures_sans_sms',
				'label' => '<:sms_liste:configuration_heure_debut_fin_sms:>',
				'explication' => '<:sms_liste:configuration_explication_heure_sms:>',
				'cacher_option_intro' => 'oui',
			],
		],
		[
			'saisie' => 'choix_grille',
			'options' => [
				'nom' => 'jours_sans_sms',
				'label' => '<:sms_liste:configuration_jours_prohibes:>',
				'explication' => '<:sms_liste:configuration_explication_jours_sms:>',
				'data_cols' => [
					'0' => '<:date_jour_1_abbr:>',
					'1' => '<:date_jour_2_abbr:>',
					'2' => '<:date_jour_3_abbr:>',
					'3' => '<:date_jour_4_abbr:>',
					'4' => '<:date_jour_5_abbr:>',
					'5' => '<:date_jour_6_abbr:>',
					'6' => '<:date_jour_7_abbr:>',
				],
				'data_rows' => [
					'0' => 'Prohibé',
				],
				'multiple' => 'oui',
			],
		],
		[
			'saisie' => 'input',
			'options' => [
				'nom' => 'obsolescence',
				'label' => '<:sms_liste:configuration_obsolescence:>',
				'explication' => '<:sms_liste:configuration_explication_obsolescence:>',
			],
			'verifier' => [
				'type' => 'entier',
				'options' => [
					'min' => 1,
					'max' => 365
				]
			]
		],
	];
	return $saisies;
}