<?php
/**
 * Gestion du formulaire d'édition d'un contact
 *
 * Permet de créer et de modifier le contact d'une liste du prestataire.
 *
 * @plugin sms_avec_liste
 * @license 2022
 * @author Vincent CALLIES
 * 
 * @package SPIP\Sms_avec_liste\Formulaires
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Saisies d'un contact
 *
 * @param string $idcontact
 *     Identifiant du contact. 'new' pour un nouveau contact.
 *     Il n'est pas utilisé d'underscore, car par sécurité un intval est fait dans ce cas,
 *     or, la valeur est un string
 * @param int|string $id_liste
 *     Identifiant de la liste parente
 * @param string $redirect
 *     URL de redirection après le traitement
 * @param string $options
 *     élements complémentaires utiles au formulaire
 * @return array
 *     Environnement du formulaire
 */
function formulaires_editer_sms_contact_saisies_dist($idcontact = 'new', $id_liste ='', $redirect = '', $options = []) {
	

	$saisies =[
		[
			'saisie' => 'input',
			'options' => [
				'nom' => 'destination',
				'label' => '<:sms_liste:info_contact_destination:>',
			]
		],[
			'saisie' => 'hidden',
			'options' => [
				'nom' => 'idcontact',
				'defaut' => $idcontact
			]
		],[
			'saisie' => 'hidden',
			'options' => [
				'nom' => 'id_liste',
				'defaut' => $id_liste
			]
		],[
			'saisie' => 'input',
			'options' => [
				'nom' => 'info1',
				'label' => '<:sms_liste:info1:>',
				'defaut'=> isset($options['info1']) ?? $options['info1'],
			]
		],[
				'saisie' => 'input',
			'options' => [
				'nom' => 'info2',
				'label' => '<:sms_liste:info2:>',
				'defaut' => isset($options['info2']) ?? $options['info2'],
			]
		],[
				'saisie' => 'input',
			'options' => [
				'nom' => 'info3',
				'label' => '<:sms_liste:info3:>',
				'defaut' => isset($options['info3']) ?? $options['info3'],
			]
		],[
				'saisie' => 'input',
			'options' => [
				'nom' => 'info4',
				'label' => '<:sms_liste:info4:>',
				'defaut' => isset($options['info4']) ?? $options['info4'],
			]
		]
	];

	return $saisies;
}

/**
 * Chargement du formulaire d'édition d'un contact
 *
 * Le squelette ../sms_avec_listes/prive/squelettes/contenu/sms_contact_edit.html
 * transmets les valeurs d'environnement à la fonction.
 *
 * @param string $idcontact
 *     Identifiant du contact. 'new' pour un nouveau contact.
 *     Il n'est pas utilisé d'underscore, car par sécurité un intval est fait dans ce cas,
 *     or, la valeur est un string
 * @param int|string $id_liste
 *     Identifiant de la liste parente
 * @param string $redirect
 *     URL de redirection après le traitement
 * @param string $options
 *     élements complémentaires utiles au formulaire
 * @return array
 *     Environnement du formulaire
 */
function formulaires_editer_sms_contact_charger_dist($idcontact = 'new', $id_liste = '', $redirect = '', $options = []) {

	$valeurs = [];
	// on vérifie la validité de l'identifiant de l'id_liste transmis
	if (
		$id = intval(_request('id_liste'))
		and $lister_sms = charger_fonction('lister_sms', 'inc')
		and $retour = $lister_sms('annuaire')
		and $retour['message'] == 'OK'
		and $ids = array_column($retour['lists'],'id')
		and in_array($id_liste, $ids)
	) {
		// la requete nous a permis de vérifier la validité de l'identifiant transmis
		$valeurs['id_liste'] = $id_liste;
		
	} else {
		// l'indentifiant n'a pas été trouvé dans l'annuaire (soit il n'y est pas, soit la requete a échoué).
		$valeurs['message_erreur'] = _T('sms_liste:err_annuaire_identifiant_existant',['id'=>$id_liste]);
		$valeurs['editable'] = false;
	}
	// on charge les valeurs de l'idcontact
	if (
		!in_array($idcontact,['oui','new',''])
		and $retour = $lister_sms('lister',[$id_liste])
		and $retour['message'] == 'OK'
	) {
		foreach ($retour['list'] as $key => $value) {
			if ($value['id'] === $idcontact) {
				$valeurs['idcontact'] = $idcontact;
				$valeurs['destination'] = $value['destination'];
				$valeurs['info1'] = $value['info1'];
				$valeurs['info2'] = $value['info2'];
				$valeurs['info3'] = $value['info3'];
				$valeurs['info4'] = $value['info4'];
			}
		}
		if (!isset($valeurs['destination'])){
			// l'indentifiant n'a pas été trouvé dans la liste (soit il n'y est pas, soit la requete a échoué).
			$valeurs['message_erreur'] = _T('sms_liste:err_liste_identifiant_existant',['id'=>$idcontact]);
			$valeurs['editable'] = false;
		}
	}

	return $valeurs;
}

/**
 * Traitements du formulaire d'édition d'un contact
 * 
 * @param string $idcontact
 *     Identifiant du contact. 'new' pour un nouveau contact.
 *     Il n'est pas utilisé d'underscore, car par sécurité un intval est fait dans ce cas,
 *     or, la valeur est un string
 * @param int|string $id_liste
 *     Identifiant de la liste parente
 * @param string $redirect
 *     URL de redirection après le traitement
 * @param string $options
 *     élements complémentaires utiles au formulaire
 * @return array
 *     Environnement du formulaire
 */
function formulaires_editer_sms_contact_traiter_dist($idcontact = 'new', $id_liste = '', $redirect = '', $options = []) {

	// retrouver les valeurs de l'environnement
	$valeurs['info1'] = _request('info1');
	$valeurs['info2'] = _request('info2');
	$valeurs['info3'] = _request('info3');
	$valeurs['info4'] = _request('info4');
	// création d'un contact
	if (in_array($idcontact,['','new','oui'])){
		$instruction = 'creer_contact';
		$valeurs['value'] = _request('destination'); // attention aux clés : value en création
		$id = [$id_liste]; // on crée dans une liste (dont on donne l'id)
		$options = ['contacts' => [$valeurs]];
	// modification d'un contact
	} else {
		$instruction = 'modifier_contact';
		$valeurs['destination'] = _request('destination');
		$id = [$idcontact]; // on modifie un contact (dont on donne l'id)
		$options = ['contacts' => $valeurs];
	}
	$lister_sms = charger_fonction('lister_sms', 'inc');
	// appel de la fonction principale pour interagir avec les listes via l'API du prestataire
	$_retours = $lister_sms($instruction, $id, $options);
	// analyse du resultat de la requête
	if ($_retours['message'] == 'OK'){
		if ($instruction == 'creer_contact'){
			$retours['message_ok'] = _T('sms_liste:ok_idcontact_creer', ['id' => $_retours['id']]);
			// nombre de contact
			if ($_retours['contacts'] == 0){
				$retours['message_ok'] .= ' ' .  _T('sms_liste:info_aucun_contact') . '.';
			} elseif ($_retours['contacts'] == 1){
				$retours['message_ok'] .= ' ' .  _T('sms_liste:info_1_contact') . '.';
			} elseif ($_retours['contacts'] > 1){
				$retours['message_ok'] .= ' ' .  _T('sms_liste:info_nb_contacts', ['nb' => $_retours['contacts']]) . '.';
			}
		} else {
			$retours['message_ok'] = _T('sms_liste:ok_idcontact_modifier');
		}
	} else {
		// il y a une erreur qu'il faut expliciter...
		$retours['message_erreur'] = $_retours['message'];
		// ...et rendre précise dans le log en transmettant les options utilisées (si on est en DEBUG)
		spip_log($_retours['message'] . print_r($options,true), 'sms_liste.' . _LOG_DEBUG);
		$retours['editable'] = false;
	}

	return $retours;
}