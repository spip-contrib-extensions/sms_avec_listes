<?php
/**
 * Gestion du formulaire pour l'envoi d'un SMS
 *
 * @plugin	   sms_avec_listes
 * @copyright  2022
 * @author	   Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\Sms_avec_listes\Formulaires
 *
 * pour l'envoi d'un SMS ($type = 'tel')
 *   si _TEST_SMS_DEST sollicite la substitution de numéros,
 *   un message d'alerte apparait dans ../prive/squelettes/contenu/sms_envoi.html
 *   la substitution des numéros de téléphone aura lieu dans ..inc/inc_envoyer_sms_dist()
 *   (rien à faire dans le présent formulaire)
 */

// sécurité
if (!defined('_ECRIRE_INC_VERSION')) return;

/**
 * Saisies du formulaire avec le plugin Saisies pour formulaires
 *
 * @link https://contrib.spip.net/Saisies
 *
 * @param string      $type  
 *                     `tel`   : valeur (prise par défaut) précisant que le SMS sera envoyé à un numéro de téléphone portable
 *                     `liste` : valeur précisant que le SMS sera envoyé à une liste de numéros
 * @param int|string  $id 
 *                      int : identifiant unique de la liste (il s'agit de celui donné par le site du prestataire)
 *                      string : numéro de téléphone portable (un int n'aimerait pas le zéro devant)
 * @param string|void $message 
 *                      string : message constitutif du SMS
 * @param string|void $redirect 
 *                      string : URL de redirection après le traitement
 * @param string|void $associer_objet 
 *                      déclaration d'une association à un objet sous la forme objet|id
 */
function formulaires_envoyer_sms_saisies_dist($type='tel', $id='', $message='', $redirect = '', $associer_objet='') {
	
	// c'est un SMS classic
	if ($type == 'tel'){
		// on a un $id
		if (intval($id)){
			$saisies[] = 
			[
				'saisie' => 'hidden',
				'options' => [
					'nom' => 'tel',
					'defaut' => $id,
				]
			];
		// on a pas d'$id, on demande le numéro de téléphone portable
		} else{
			$saisies[] = 
			[
				'saisie' => 'input',
				'options' => [
					'label' => '<:sms_liste:info_destination:>',
					'nom' => 'tel',
					'defaut' => $id,
				]
			];
		}
	// c'est un SMS à une liste
	} elseif ($type == 'liste'){
		// on a un $id
		if (intval($id)){
			$saisies[] = 
			[
				'saisie' => 'hidden',
				'options' => [
					'nom' => 'id_liste',
					'defaut' => $id,
				]
			];
		// on a pas d'$id, on demande l'id de la liste au travers d'un menu déroulant
		} elseif ($lister_sms = charger_fonction('lister_sms', 'inc')){
			$retour = $lister_sms('annuaire');
			if ($retour['message'] == 'OK'){
				// créer un tableau clé=$id et valeur=name
				$data=[];
				foreach ($retour['lists'] as $nb => $valeurs) {
					$data[$valeurs['id']] = $valeurs['name'] . ' (' . $valeurs['count'] . ')';
				}
				$saisies[] = 
				[
					'saisie' => 'selection',
					'options' => [
						'label' => '<:sms_liste:info_listes:>',
						'nom' => 'id_liste',
						'data' => $data,
						'defaut' => $id,
						'cacher_option_intro' => 'oui',
					]
				];
			}
		}
		// une liste demande des paramètres supplémentaires
		$saisies[] = 
		[
			'saisie' => 'case',
			'options' => [
				'label' => '<:sms_liste:info_simulate:>',
				'nom' => 'simulate',
			]
		];
		$saisies[] = 
		[
			'saisie' => 'radio',
			'options' => [
				'label' => '<:sms_liste:info_pushtype:>',
				'nom' => 'pushtype',
				'aide' => 'smsqualification',
				'data' => 
				[
					'alert' => '<:sms_liste:info_alerte:>',
					'marketing' => '<:sms_liste:info_marketing:>',
				],
				'defaut' => 'alert',
			]
		];
		$saisies[] = 
		[
			'saisie' => 'radio',
			'options' => [
				'nom' => 'choix_delay',
				'label' => '<:sms_liste:texte_choix_delay:>',
				'aide' => 'smstemporalite',
				'data' => 
				[
					'asap' => '<:sms_liste:info_asap:>',
					'delay' => '<:sms_liste:info_delay:>',
				],
				'defaut' => 'asap',
				],
		];
		$saisies[] = 
		[
			'saisie' => 'date',
			'options' => [
				'nom' => 'delay',
				'label' => '<:sms_liste:texte_delay:>',
				'explication' => '<:sms_liste:texte_delay_explication:>',
				'afficher_si' => '@choix_delay@ == "delay"',
			],
			'verifier' => [
				'type' => 'date',
				'options' => [
					'normaliser' => 'datetime'
				],
			],
		];
	}

	// un peu de javascript pour permettre un décompte des caractères du SMS
	$javascript = "<script>
	function count_down(obj) {
		var element = document.getElementById('smscar');
		element.innerHTML = obj.value.length;
		if (obj.value.length > 160) {
			element.style.color = 'red';
		} else {
			element.style.color = 'grey';
		}    
	}
	</script>";
	$spans = "<span><span id='smscar' style='color: grey;'>0</span>/160</span>";
	$attribut = 'onkeyup="count_down(this);"';

	// une zone de texte pour taper le message du SMS
	$saisies[] = [
		'saisie' => 'textarea',
		'options' => [
			'nom' => 'message',
			'label' => _T('sms_liste:info_message'),
			'defaut' => $message,
			'aide' => 'smsmessage',
			'inserer_debut' => $javascript,
			'inserer_fin' => $spans,
			'attributs' => $attribut,
		]
	];

	# A-t-on demandé à ce que le SMS soit associé à un objet éditorial ?
	if ($associer_objet) {
		// l'association n'a de sens que s'il y a un job à associer à l'objet
		// le cas échéant, l'administrateur doit pouvoir briser l'association
		if (
			include_spip('inc/config') and $envoi = lire_config('sms_avec_liste/envoi','immediat')
			and $envoi === 'job' and isset($GLOBALS['visiteur_session']['statut']) and $GLOBALS['visiteur_session']['statut'] === '0minirezo'
		) {
			$saisies[] = [ // le fieldset
				'saisie' => 'fieldset',
				'options' => [
					'nom' => 'lassociation',
					'label' => _T ('sms_liste:info_lassociation'),
				],
				'saisies' => [ // les champs dans le second fieldset
					[ // hors fieldset : association
						'saisie' => 'oui_non',
						'options' => [
							'nom' => 'c_associe',
							'label' => _T('sms_liste:texte_associer_sms'),
							'valeur_oui' => 'oui',
							'valeur_non' => 'non',
							'defaut' => 'oui'
						]
					],
				]
			];
		}
	}
	return $saisies;
}

/**
 * Chargement du formulaire
 *
 * @uses lister_sms
 * @uses API Verifier
 *
 * @param string      $type  
 *                     `tel`   : valeur (par défaut) précisant que le SMS sera envoyé à un numéro de téléphone portable
 *                     `liste` : valeur précisant que le SMS sera envoyé à une liste de numéros
 * @param int|string  $id 
 *                      int : identifiant unique de la liste (il s'agit de celui donné par le site du prestataire)
 *                      string : numéro de téléphone portable (un int n'aimerait pas le zéro devant)
 * @param string|void $message 
 *                      string : message constitutif du SMS
 * @param string|void $redirect 
 *                      string : URL de redirection après le traitement
 * @param string|void $associer_objet 
 *                      déclaration d'une association à un objet sous la forme objet|id
 */
function formulaires_envoyer_sms_charger_dist($type='tel', $id='', $message='', $redirect = '', $associer_objet='') {
	$valeurs = [];
	if ($type === 'tel' and $id and !in_array($id,['new','oui'])) {
		// il faut vérifier la validité des numéros transmis
		$verifier = charger_fonction('verifier', 'inc/');
		$type_de_test = 'numero_e164';
		$options_enventuelles = ['pays' => 'fr','normaliser' => 'oui', 'normalisation_prefix' => '33', 'seulement' => 'portable'];
		if ($erreur = $verifier($id, $type_de_test, $options_enventuelles, $id)){
			$valeurs['message_erreur'] = $erreur;
			$valeurs['editable'] = false;
		}
	}
	if ($type === 'liste' and $id) {
		// il faut vérifier la validité de l'identifiant de la liste
		if ($lister_sms = charger_fonction('lister_sms', 'inc')
		  and $retour = $lister_sms('annuaire')
		  and $retour['message'] == 'OK'
		  and $ids = array_column($retour['lists'],'id')
		  and in_array($id, $ids)
		){
			
		} else {
			$valeurs['message_erreur'] = _T('sms_liste:err_annuaire_identifiant_existant',['id' => $id]);
			$valeurs['editable'] = false;
		}
	}
	return $valeurs;
}

/**
 * Traitement du formulaire.
 *
 * @uses envoyer_sms()
 * @uses lister_sms()
 * @uses job_queue_add()
 * @uses job_queue_link()
 *
 * @param string      $type  
 *                     `tel`   : valeur (par défaut) précisant que le SMS sera envoyé à un numéro de téléphone portable
 *                     `liste` : valeur précisant que le SMS sera envoyé à une liste de numéros
 * @param int|string  $id 
 *                      int : identifiant unique de la liste (il s'agit de celui donné par le site du prestataire)
 *                      string : numéro de téléphone portable (un int n'aimerait pas le zéro devant)
 * @param string|void $message 
 *                      string : message constitutif du SMS
 * @param string|void $redirect 
 *                      string : URL de redirection après le traitement
 * @param string|void $associer_objet 
 *                      déclaration d'une association à un objet sous la forme objet|id
 * @return array
 */
function formulaires_envoyer_sms_traiter_dist($type='tel', $id='', $message='', $redirect = '', $associer_objet='') {
	$erreurs = array();
	// la requete d'environnement peut être différente de la variable de la fonction si le formulaire l'a changée.
	$message = _request('message');
	$options = [];

	switch ($type) {
		case 'liste':
			$id_liste = _request('id_liste');
			// le string est convertit en tableau (et si on peut faire une cellule par segment séparable par une vigule, on le fait).
			$id_liste = explode(",", $id_liste);
			$options['message'] = $message;
			$options['pushtype'] = _request('pushtype');
			$options['type_sms'] = _request('simulate');
			if (_request('choix_delay') === 'delay'){
				$options['delay'] = _request('delay');;
			}
			$envoi = lire_config('sms_avec_listes/envoi','immediat');
			// envoi immédiat
			if ($envoi === 'immediat') {
				// chargement et appel de la fonction principale `lister_sms()`
				if ( $envoyer_sms = charger_fonction('envoyer_sms', 'inc') ){
					if ( $lister_sms('envoyer', [$id_liste], $options) ){
						$erreurs['message_ok'] = _T('sms_liste:ok_message');
					} else {
						$erreurs['message_erreur'] = _T('sms_liste:err_message');
						return $erreurs;
					}
				} else {
					$erreurs['message_erreur'] = _T('sms_liste:err_fonc_envoyer_sms');
					return $erreurs;
				}
			} 
			if ($envoi === 'job') {
				// définir le moment de l'envoi selon la configuration ou, à defaut, au premier choix de la constante (dans 5 minutes normalement)
				if (!$temporisation = lire_config('sms_avec_listes/temporisation','')){
					$temporisation = _DELAIS_DU_JOB_ENVOYER_SMS;
					$temporisation = $temporisation[0];
				}
				$time = strtotime($temporisation);
				// la description du job
				$description = 'SMS -> liste(s) n° ' . implode(',', $id_liste) . " Texte : $message. Demande auteur n°" . $GLOBALS['visiteur_session']['id_auteur'] . ' (' . $GLOBALS['visiteur_session']['nom'] . ').';
				// le job
				$id_job = job_queue_add(
					'lister_sms',
					$description,
					['envoyer', $id_liste, $options],
					'inc/',
					true,
					$time
				);
			}
			break;
		case 'tel':
			$tel = [_request('tel')];
			include_spip('inc/config');
			$envoi = lire_config('sms_avec_listes/envoi','immediat');
			// envoi immédiat
			if ($envoi === 'immediat') {
				// chargement et appel de la fonction principale `envoyer_sms()`
				if ( $envoyer_sms = charger_fonction('envoyer_sms', 'inc') ){
					if ( $envoyer_sms($message, $tel, $options) ){
						$erreurs['message_ok'] = _T('sms_liste:ok_message');
					} else {
						$erreurs['message_erreur'] = _T('sms_liste:err_message');
					}
				} else {
					$erreurs['message_erreur'] = _T('sms_liste:err_fonc_envoyer_sms');
				}
			}
			// envoi en job
			if ($envoi === 'job') {
				// la description du job
				$description = 'SMS à : ' . implode(',', $tel) . " Texte : $message. Demande auteur n°" . $GLOBALS['visiteur_session']['id_auteur'] . ' (' . $GLOBALS['visiteur_session']['nom'] . ').';
				// définir l'envoi selon la configuration ou, à defaut, au premier choix de la constante (dans 5 minutes normalement)
				if (!$temporisation = lire_config('sms_avec_listes/temporisation','')){
					$temporisation = _DELAIS_DU_JOB_ENVOYER_SMS;
					$temporisation = $temporisation[0];
				}
				$time = strtotime($temporisation);
				// le job
				$id_job = job_queue_add(
					'envoyer_sms',
					$description,
					[$message, $tel, $options],
					'inc/',
					true,
					$time
				);
			}
			break;
		default:
			$erreurs['message_erreur'] = _T('sms_liste:err_modalite_sms');
	}

	// traitement commun du job, qu'il soit pour un SMS sur une liste ou sur un n°
	if ($envoi === 'job') {
		// l'association du job à un objet
		if ($id_job and _request('c_associe') === 'oui' and ($associer_objet)){
			list($objet,$id_objet) = explode('|', $associer_objet);
			// on verifie que l'objet et son identifiant existe
			$verifier = charger_fonction('verifier', 'inc/');
			$erreur_objet = $verifier($id_objet, $type_de_test, ['objet' => $objet]);
			if (!erreur_objet){
				// Lorsque des travaux sont associés à un objet, ils apparaissent sur la page de l’espace privé de cet objet,
				// et les auteurs qui ont le droit de modifier l’objet peuvent aussi supprimer le travail avant son exécution.
				job_queue_link($id_job, array(array('objet' => $objet, 'id_objet' => $id_objet)));
			}
		}
		// l'information sur le traitement
		$etape1 = ''; $etape2 = ''; $erreur = true;
		if ($id_job){
			$etape1 = _T('sms_liste:ok_message_job', ['id' => $id_job]);
			$erreur = false;
			if (isset($erreur_objet) and $erreur_objet){
				$etape2 = _T('sms_liste:err_message_job_asso', ['id' => $id_objet, 'obj' => $objet, 'err' => $erreur_objet]);
				$erreur = true;
			} elseif (isset($erreur_objet)) {
				$etape2 = _T('sms_liste:ok_message_job_asso', ['id' => $id_objet, 'obj' => $objet]);
			}
		} else {
			$etape1 = _T('sms_liste:err_message_job');
			$erreur = true;
		}
		if ($erreur) {
			$erreurs['message_erreur'] = $etape1 . ' ' . $etape2;
		} else {
			$erreurs['message_ok'] = $etape1 . ' ' . $etape2;
		}
		// la tâche demandée au Cron doit être dans le log.
		spip_log($description . ' Temporisation : '.$temporisation, 'sms_liste.' . _LOG_DEBUG);
	}
	return $erreurs;
}

