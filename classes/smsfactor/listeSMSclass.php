<?php

// sécurité
if (!defined('_ECRIRE_INC_VERSION')) { return;
}


class ListeSMSclass
{
	//input parameters ----------------
	var $token = FALSE; // token
	var $instruction = ''; // instruction relative à la liste (création...)
	var $ids = []; // ids de listes ou de contacts
	var $options = []; // tableau d'informations accompagnant l'instruction

	// variables ne servant que dans la classe, on met leurs propriétés en privé.
	private string $host; // URL
	private array $optional_headers; // headers de la requête
	private array $tableauFormate = []; // tableau de données avant qu'il ne soit formaté et envoyé en data par la requete
	private string $request_data; // data de la requête
	private string $response; // réponse à la requête
	private string $methode; // méthode de la requête
	private string $nom; // le nom de la liste
	private string $message; // le message qui sera envoyé à la liste
	private array $contacts; // des numéros et des informations personnalisées pour la liste
	private string $id_liste; // identifiant unique de la liste
	private string $delay; // date de l'envoi du SMS par la liste
	
	function ListeSMS($token, $instruction, $ids=[], $options=[]){
		$this->token = $token;
		$this->instruction = $instruction;
		$this->ids = $ids;
		$this->options = $options;
		// definir un header pour accueillir le json et donner le token
		$headers = ['Content-Type: application/json', 'Accept: application/json', "Authorization: Bearer " . $token];

		// Cette commande vous permet de créer et de peupler une liste.
		// @link https://dev.smsfactor.com/en/api/sms/list/create-list
		if ($instruction == 'creer'){
			$this->contacts = $options['contacts'];
			$this->nom = $options['name'];
			$this->CreerListeContacts();
			$dir = '/list';
		}
		// Cette commande vous permet d'avoir une liste des listes.
		// @link https://dev.smsfactor.com/en/api/sms/list/get-lists
		if ($instruction == 'annuaire'){
			$this->AnnuaireListes();
			$dir = '/lists';
		}
		// Cette commande vous permet de lire une liste de contacts.
		// @link https://dev.smsfactor.com/en/api/sms/list/get-list
		if ($instruction == 'lister'){
			$this->ListerListe();
			$dir = '/list/'.$ids[0];
		}
		// Cette commande vous permet d'ajouter le NPAI de votre liste d'envoi
		// dans votre liste NPAI et de supprimer le NPAI dans la liste d'envoi.
		// @link https://dev.smsfactor.com/en/api/sms/list/clear
		if ($instruction == 'nettoyer'){
			$this->ViderListe();
			$dir = "/list/$ids[0]/npai/clear";
		}
		// La phrase « n'habite plus à l'adresse indiquée » (NPAI),
		// esignifie que le SMS ne peut parvenir à destination
		// Cette commande vous permet de récupérer votre liste NPAI. 
		if ($instruction == 'npai'){
			$this->ListeNpai();
			$dir = "/npai";
		}
		// Cette commande vous permet de détruire une liste de contacts. 
		// @link https://dev.smsfactor.com/en/api/sms/list/delete-list
		if ($instruction == 'supprimer_liste'){
			$this->SuppressionListe();
			$dir = "/list/$ids[0]";
		}
		// Cette commande vous permet de récupérer votre liste noire. 
		// Un contact qui répond STOP ou se désabonne via le lien STOP
		// sera automatiquement déplacé dans votre liste noire.
		// @link https://dev.smsfactor.com/en/api/sms/list/get-blacklist
		if ($instruction == 'liste_noire'){
			$this->ListeNoir();
			$dir = "/blacklist";
		}
		// Cette commande vous permet de supprimer un contact d'une liste. 
		if ($instruction == 'supprimer_contact'){
			$this->SuppressionContact();
			$dir = "/list/contact/$ids[0]";
		}
		// Cette commande vous permet de créer un contact ou des contacts dans une liste. 
		if ($instruction == 'creer_contact'){
			$this->id_liste = $ids[0];
			$this->contacts = $options['contacts'];
			$this->CreerContact();
			$dir = "/list";
		}
		// Cette commande vous permet de modifier un contact ou des contacts dans une liste. 
		if ($instruction == 'modifier_contact'){
			$this->contacts = $options['contacts'];
			$this->ModifierContact();
			$dir = "/list/contact/$ids[0]";
		}
		// Cette commande vous permet d'envoyer un SMS à tous les contacts d'une liste ou de plusieurs listes. 
		// @link https://dev.smsfactor.com/en/api/sms/send/send-list
		if ($instruction == 'envoyer'){
			// reformater les $ids avec une clé value
			$ids_reformat=[];
			foreach ($ids as $key => $value) {
				$ids_reformat[]=['value' => $value];
			}
			$this->ids=$ids_reformat;
			$this->message = $options['message'];
			$this->delay = $options['delay'];
			$this->EnvoyerSMS();
			$dir = "/send/lists";
			if (isset($options['type_sms']) and $options['type_sms'] === 'simulate'){
				$dir .= '/simulate';
			}
			spip_log('listeSMSclass dir :' . $dir, 'sms_liste.' . _LOG_DEBUG);
		}
		// Cette commande vous permet de connaitre l'état de vos crédits chez le prestataire. 
		if ($instruction == 'credits'){
			$this->AvoirCredits();
			$dir = "/credits";
		}

		$this->optional_headers = $headers;
		$this->host = "https://api.smsfactor.com" . $dir;
		$this->response = $this->do_post_request($this->methode, $this->host, $this->request_data, $this->optional_headers);
		return $this->response;
	}

	function CreerListeContacts()
	{
		$formate =  [
			'list' => [
				'name' => $this->nom,
				'contacts' => [
					'gsm' => $this->contacts
				]
			]
		];
		$this->methode = 'POST';
		$this->tableauFormate = $formate;
		$this->request_data =  json_encode($this->tableauFormate);
	}

	function AnnuaireListes()
	{
		$this->methode = 'GET';
		$this->request_data =  '';
	}

	function ListerListe()
	{
		$this->methode = 'GET';
		$this->request_data =  '';
	}

	function ViderListe()
	{
		$this->methode = 'PUT';
		$this->request_data =  '';
	}

	function SuppressionListe()
	{
		$this->methode = 'DELETE';
		$this->request_data =  '';
	}

	function ListeNoir()
	{
		$this->methode = 'GET';
		$this->request_data =  '';
	}

	function ListeNpai()
	{
		$this->methode = 'GET';
		$this->request_data =  '';
	}

	function SuppressionContact()
	{
		$this->methode = 'DELETE';
		$this->request_data =  '';
	}

	function CreerContact()
	{
		$formate = [
			'list' => [
				'listId' => $this->id_liste,
				'contacts' => [
					'gsm' => $this->contacts
				]
			]
		];
		$this->methode = 'POST';
		$this->tableauFormate = $formate;
		$this->request_data =  json_encode($this->tableauFormate);
	}

	function ModifierContact()
	{
		$formate = $this->contacts;
		$this->methode = 'PUT';
		$this->tableauFormate = $formate;
		$this->request_data =  json_encode($this->tableauFormate);
	}

	function EnvoyerSMS()
	{
		$formate =  [
			'sms' => [
				'message' => 
				[
					'text' => $this->texte_avec_liens_courts(),
					'pushtype' => 'alert',
					'sender' => 'SYNPER',
					'delay' => $this->delay,
					'unicode' => 0,
				],
				'lists' => $this->ids
			]
		];
		spip_log('listeSMSclass :' . print_r($formate,true) , 'sms_liste.' . _LOG_DEBUG);
		$this->methode = 'POST';
		$this->tableauFormate = $formate;
		$this->request_data =  json_encode($this->tableauFormate);
	}
	function AvoirCredits()
	{
		$this->methode = 'GET';
		$this->request_data =  '';
	}
	
/**
 * Fonction procèdant à la requête
 *
 * @param string $methode
 *        cURL prend en charge les méthodes de requêtes en HEAD, GET, POST, PUT...
 *        La gestion des listes procède en utilisant différentes méthodes précisées dans ce paramètre
 * @param string $url
 *        URL de l'API
 * @param json $postdata
 *        arguments
 * @param array $optional_headers
 *        headers
 * @return json
 *        retour de la requete faite
 */
	private function do_post_request($methode, $url, $postdata, $optional_headers = null)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		if ($methode == 'POST'){
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
		}
		if ($methode == 'PUT'){
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
			if ($postdata){
				curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
			}
		}
		if ($methode == 'DELETE'){
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
		}
		curl_setopt($ch, CURLOPT_HTTPHEADER, $optional_headers);
		$response = curl_exec($ch);
		curl_close($ch);
		return $response;
	}

/**
 * Méthode privée pour permettre l'usage de liens courts
 *
 * Il faut identifier les liens dans le texte et les substituer par le tag prévu ```<-short->```,
 * puis indiquer à l'API les liens devant être raccourcis par un tableau links.
 * L'API les calculera et les placera à la place du tag ```<-short->```.
 *
 * @link https://dev.smsfactor.com/fr/api/sms/envoi/liens-courts
 *
 * @uses string  $this->message
 *                       Le texte du SMS
 * @return array  $message
 *                       Un nouveau formatage du texte en tableau 'text' s'il y a des liens 'links'
 */
	private function texte_avec_liens_courts() {
		// Masque
		$regex = '@([http|ftp|https]+://[a-z0-9?=:&\./+,%#_-]+)@i';
		$links = [];
		$text = $this->message;
		preg_match_all($regex, $text, $trouvaille, PREG_SET_ORDER);
		if ($trouvaille){
			foreach ($trouvaille as $key => $value) {
				$links[] = $value[0];
				$text = str_replace($value[0], '<-short->', $text);
			}
			return ['text' => $text, 'links' => $links];
		}
		return $text;
	}
}