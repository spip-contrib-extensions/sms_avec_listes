<?php
/**
 * Ajout d'une vérification à l'API Vérifier
 *
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Vérifier que l'information respecte les normes E.164
 * 
 * @link https://www.itu.int/rec/T-REC-E.164-201011-I/fr
 *
 *
 * @param string $valeur
 *   Numéro de téléphone portable. Plusieurs numéros peuvent être séparés par des virgules.
 * @param array $options
 *   précisions accompagnant la transmission du numéro.
 *   -- pays : précise le pays dont relève le numéro
 *   -- normaliser : précise si le numéro doit être normalisé
 *   -- normalisation_prefix : précise un prefix non usuel pour la normalisation
 *      la normalisation souhaitée par SMSFactor est avec un préfix 33 (exemple: 33612345678).
 *      il faut solliciter cette atypicité en la précisant par l'option ’normalisation_prefix’
 *   -- normalisation_esthetique : presente le numéro avec des espaces facilitant sa lecture (exemple: +33 6 12 34 56 78)
 *   -- seulement : fixe | portable : permet de vérifier si le numéro répond à un type précis de numéro
 * @param null $valeur_normalisee
 *   Si normalisation a faire, la variable sera rempli par le texte corrigé.
 * @return string
 *   Retourne une chaine vide si c'est valide, sinon une chaine expliquant l'erreur.
 **/

function verifier_numero_e164_dist($valeur, $options=[], &$valeur_normalisee = null) {
	if (!empty($options['message_erreur_defaut'])) {
		$erreur = $options['message_erreur_defaut'];
	}
	else {
		$erreur = _T('sms_liste:err_gms');
	}
	if (!is_string($valeur)) {
		return $erreur;
	}
	// transformer la chaine en tableau
	if (isset($options['pluralite']) and $options['pluralite'] === 'oui'){
		// les numéros peuvent être un string dont chaque numéro est partagé par une vigule
		if (strpos($valeur, ',')){
			$tels = explode(',', $valeur);
		} else {
			$tels = [$valeur];
		}
	} else {
		$tels = [$valeur];
	}
	// verifier le tableau et le normaliser si nécessaire
	foreach ($tels as $ordre => $_tel) {
		// On ne veut que des chiffres, on retire donc les points, les slashes, les tirets, les espaces. 
		$gsm = preg_replace('#\.|/|-| #i', '', $_tel);
		// Fr 
		if (isset($options['pays']) and $options['pays'] == 'fr'){
			// Vérifier que l'on a un numéro conforme :
			// Pour les prefixes, on accepte les notations +33 et 0033 ou simplement le 0
			// et on veut que le numéro débute par 6 ou 7 puisque c'est un portable
			if (isset($options['seulement']) and $options['seulement'] === 'portable'){
				$regex = '#^(\+33|0|0033)([6-7]\d{8})$#';
			} elseif (isset($options['seulement']) and $options['seulement'] === 'fixe') {
			// 5 zones géographiques métropolitaines :
			// 01 : Région Île-de-France. 02 : Région Nord-Ouest. 03 : Région Nord-Est. 04 : Région Sud-Est. 05 : Région Sud-Ouest
				$regex = '#^(\+33|0|0033)([1-5]\d{8})$#';
			}else {
				$regex = '#^(\+33|0|0033)([1-9]\d{8})$#';
			}
			if (preg_match($regex , $gsm, $resultats)) {
				// $resultats 
				// -- dans sa clé 0 contient le numéro,
				// -- sa clé 1 le prefix 
				// -- et dans sa clé 2 le numéro sans le prefix
				// on normalise le tableau si demandé. 
				if ($options['normaliser'] and $options['normaliser'] === 'oui') {
					if (isset($options['normalisation_esthetique']) and $options['normalisation_esthetique'] === 'oui'){
						$resultats[2] = '&nbsp;' . substr($resultats[2],0,1) . '&nbsp;' . wordwrap(substr($resultats[2],1),2,"&nbsp;",1);
					}
					if (isset($options['normalisation_prefix'])){
						$tels[$ordre] = $options['normalisation_prefix'] . $resultats[2];
					} else {
						$tels[$ordre] = '+33' . $resultats[2];
					}
				} 
			} else {
				// s'il y a plusieurs numéros, faciliter l'identification de celui qui est non conforme
				if ($ordre > 1){
					$erreur .= " (rang $ordre)";
				}
				return $erreur;
			}
		}
		// on retourne le tableau normalisé en chaine si nécessaire
		if ($options['normaliser'] and $options['normaliser'] == 'oui') {
			$valeur_normalisee = implode(',', $tels);
		}
	}
	return '';
}