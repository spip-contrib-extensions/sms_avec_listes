<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'smsmessage' => 'Les message SMS',
	'smsmessage_longueur' => 'La longueur',
	'smsmessage_caracteres' => 'Les caractères',
	'smsmessage_champs' => 'Les champs',
	'listemaintenance' => 'La maintenance des listes',
	'listemaintenance_objets' => 'Les objets SPIP',
	'listemaintenance_numeros' => 'Les numéros des objets',
	'listemaintenance_listes' => 'Les listes chez le prestataire',
	'listemaintenance_schema' => 'Le schéma pour l’export des listes',
	'listemaintenance_peuplement' => 'La création des listes',
	'smstemporalite' => 'Temporalité',
	'smstemporalite_immediat' => 'L’envoi immédiat',
	'smstemporalite_differe' => 'L’envoi différé',
	'smsqualification' => 'La qualification juridique de vos SMS',
	'smsqualification_alerte' => 'Les alertes',
	'smsqualification_marketing' => 'Les offres commerciales',
);