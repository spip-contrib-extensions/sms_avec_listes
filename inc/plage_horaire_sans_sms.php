<?php
/**
 * Savoir que l'on est dans la plage horaire sans SMS
 *
 *   exemple avec fonction charger_fonction()
 *     ```
 *     $plage_horaire = charger_fonction('plage_horaire_sans_sms', 'inc');
 *     if ($plage_horaire()){ echo 'on ne devrait pas envoyer'; } else { echo 'on peut envoyer'; }
 *     ```
 *
 * @plugin     sms_avec_listes
 * @copyright  2022
 * @author     Vincent CALLIES
 * @licence    MIT
 * @package    SPIP\Sms_avec_listes\Inc\Plage_horaire_sans_sms
 */

// sécurité
if (!defined('_ECRIRE_INC_VERSION')) { return;
}

/**
 * Fonction retournant si l'on est dans la plage horaire sans sms
 *
 * @uses inc/config
 *
 * @param string $date par defaut ce sera la date du jour. 
 *                     Attention 'now' (relatif) se verra appliquer la tz 
 *                     mais pas 2023-01-03 23:40:00 (absolu) sera considéré comme dans la tz
 * @param string $tz pour la timezone. Par défaut, la timezone du serveur et en l'absence UTC
 *               Il est possible d'utiliser le plugin Timezone qui vous permet de définir la timezone du site
 *               et évite de devoir préciser celle-ci à chaque appel de la fonction.
 *               @link https://www.php.net/manual/en/timezones.php
 * @return boolean
 */
function inc_plage_horaire_sans_sms_dist($date='now', $tz='') {
	$retour = true;
	// Traiter la timezone si nécessaire
	if ($tz) {
		date_default_timezone_set($tz);
	}

	$heure_actuelle = date_format(date_create($date), 'H:i');
	$jour_actuel = date_format(date_create($date), 'w');

	include_spip('inc/config');
	// Sommes-nous dans une heure sans sms ?
	if ($heures = lire_config('sms_avec_listes/heures_sans_sms')) {
		if ($heure_actuelle < $heures[0] and $heure_actuelle > $heures[1] ){
			$retour = false;
		}
	} 
	// Dans la négative, sommes-nous un jour sans sms ?
	if (!$retour and $jours = lire_config('sms_avec_listes/jours_sans_sms')) {
		if (in_array($jour_actuel,$jours[0])){
			$retour = true;
		}
	}
	return $retour;
}