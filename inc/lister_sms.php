<?php
/**
 * Créer une liste,
 *   via un job :
 *     ```
 *     $id_job = job_queue_add(
 *       'lister_sms',
 *       $description,
 *       array($instruction,$id,$options),
 *       'inc/',
 *       true,
 *       $time
 *     );
 *     ```
 *   via la fonction charger_fonction()
 *     ```
 *     $lister_sms = charger_fonction('lister_sms', 'inc');
 *     $lister_sms($instruction,$id,$options);
 *     ```
 * @plugin     SMS SPIP
 * @copyright  2015
 * @author     tofulm
 * @licence    GNU/GPL
 * @package    SPIP\Sms\Inc\Lister_sms
 */

// sécurité
if (!defined('_ECRIRE_INC_VERSION')) { return;
}

include_spip('inc/config');

function inc_lister_sms_dist($instruction, $ids=[], $options=[]) {
	if (lire_config('sms/prestataire') == 'smsfactor') {
		return smsfactor_liste($instruction, $ids, $options);
	} elseif (lire_config('sms/prestataire') == 'octopush') {
		return false; // à faire
	} else {
		return false;
	}
}

/**
 * Cette fonction permet de traiter des listes. 
 *
 * Les mentions dans les logs, en debug, sont nécessaires
 * car lors d'un appel par un job, il n'y aura pas de retour avec un
 * message d'erreur.
 *
 * @param string $instruction
 *        Opération souhaitée sur la liste (exemple : creer)
 * @param array $ids
 *        Identifiant(s) unique(s) de listes ou de contacts
 * @param array $options
 *        Options accompagnant l'instruction
 * @return array
 *        Tableau indiquant le succès ou l'échec via la clé ’ok’
 *        L'explication éventuelle de l'opération via la clé ’message’
 *        Les agruments éventuels via la clé ’arg’
 */
function smsfactor_liste($instruction, $ids = [], $options = []){
	// On initialise le retour à une erreur nok
	$retour = array(
		'ok'   => false,
		'message'  => '',
		'arg'  => false,
	);
	// on analyse l'instruction
	if (!in_array($instruction, ['creer', 'annuaire', 'lister', 'nettoyer', 'supprimer_liste', 'liste_noire', 'npai', 'supprimer_contact', 'creer_contact', 'modifier_contact', 'envoyer', 'credits'])){
		$retour['message'] = _T('sms_liste:err_liste');
		spip_log("smsfactor_liste(): $instruction -> ids (" . implode(',', $ids) . ")\noptions" . print_r($options,true) . $retour['message'] , 'sms_liste.' . _LOG_DEBUG);
		return $retour;
	}
	// on analyse et formate les options nécessaires à certaines instructions
	switch ($instruction) {
		// créer un liste de contacts 
		//     avec la transmission d'un tableau à trois entrées 
		//     -- ’name’ de la liste 
		//     peut être laissée vide mais sera alors créée et portera comme nom la date où elle a été créée.
		//     -- ’contacts’
		//     Chaque contact débute par une clé ’value’ => numéro de téléphone
		//     suit jusqu'à 4 informations personnalisées (clé => valeur). 
		//     Ces informations peuvent ensuite être affichées à l'intérieur du texte du SMS envoyé à la liste. 
		//     Par exemple, si vous insérez [clé] dans le message envoyé à la liste, le tag sera automatiquement remplacé 
		//     par la valeur de la clé de l'information personnalisée accompagnant le contact.
		//     @link https://dev.smsfactor.com/en/api/sms/list/create-list
		//     -- 'verifier'
		//     Permet l'utilisation du plugin Verifier pour vérifier la conformité des numéros de téléphone portable
		//     avec la vérification donnée dans la clé ’nom’ 
		//     et ses options précisée par les valeurs de la clé ’options’
		case 'creer':
			// vérifier $options
			if (!is_array($options)){
				$retour['message'] = _T('sms_liste:err_listec');
				return $retour;
			}
			// il faut un nom de liste et des contacts
			if (!isset($options['name']) or !isset($options['contacts']) or !is_string($options['name']) or !is_array($options['contacts'])){
				$retour['message'] = _T('sms_liste:err_listec') . "\n" . _T('sms_liste:err_listec_cles');
				spip_log('smsfactor_liste():' . $retour['message'] , 'sms_liste.' . _LOG_DEBUG);
				return $retour;
			}
			// les contacts doivent proposer au moins un numéro de téléphone portable
			foreach ($options['contacts'] as $_contact => $valeurs) {
				if (!isset($valeurs['value'])){
					$retour['message'] = _T('sms_liste:err_listec') . "\n" . _T('sms_liste:err_listec_gsm');
					spip_log('smsfactor_liste():' . $retour['message'] , 'sms_liste.' . _LOG_DEBUG);
					return $retour;
				}
			}
			$args = $options;
			$ids = [];
		break;
		case 'annuaire':
			$ids = [];
		break;
		case 'envoyer':
			// il est impératif de définir le $options['pushtype'] (alert ou marketing)
			if (!isset($options['pushtype']) or !in_array($options['pushtype'], ['alert','marketing'])){
				$retour['message'] = 'Il est nécessaire de préciser le type d’envoi !';
				spip_log('smsfactor_liste():' . $retour['message'] , 'sms_liste.' . _LOG_DEBUG);
				return $retour;
			}
			//  il est impératif de définir le $options['delay'] sous la forme d'une date Y-m-d H:i:s
			if (isset($options['delay'])){
				// vérifier la validité de la date et que la date soit une date future
				$dt = DateTime::createFromFormat('Y-m-d', $options['delay']);
    			if( !$dt && $dt->format('Y-m-d') === $options['delay']
					or !$options['delay'] > date('Y-m-d H:i:s')){
					$retour['message'] = 'La date d’envoi programmée pour le SMS n’est pas valide !';
					spip_log('smsfactor_liste():' . $retour['message'] , 'sms_liste.' . _LOG_DEBUG);
					return $retour;
				}
			} else {
				// sans date, on la calcule
				$options['delay'] = date('Y-m-d H:i:s' , strtotime('+ 5 minutes'));
			}
		break;
	}
	// certaines instructions ont besoin d'un identifiant
	if ( in_array($instruction, ['lister','nettoyer','supprimer_liste','supprimer_contact','modifier_contact','envoyer']) and (!isset($ids[0])) ){
		$retour['message'] =  "pas d'id alors que l'on demande $instruction";
		spip_log('smsfactor_liste():' . $retour['message'] , 'sms_liste.' . _LOG_DEBUG);
		return $retour;
	}
	// si pas de token, inutile de solliciter l'API
	$token = $options['token'] ?? lire_config('sms/token_smsfactor');
	if (empty($token)){
		$retour['message'] = 'Il faut un token pour utiliser les listes !';
		spip_log('smsfactor_liste():' . $retour['message'] , 'sms_liste.' . _LOG_DEBUG);
		return $retour;
	}
	// tout est bien paramétré, on appelle l'API
	include_spip('classes/smsfactor/listeSMSclass');
	$LISTESMS = new ListeSMSclass();
	spip_log("argument envoyé à la classe ListeSMS: $instruction".print_r($ids,true).print_r($options,true), 'sms_liste.' . _LOG_DEBUG);
	$reponse  = $LISTESMS->ListeSMS($token, $instruction, $ids, $options);
	$reponse = json_decode($reponse,true);
	// on retourne la réponse de l'API
	spip_log('smsfactor_liste(reponse):' . print_r($reponse,true) , 'sms_liste.' . _LOG_DEBUG);
	return $reponse;
}