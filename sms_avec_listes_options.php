<?php
/**
 * fonctions, variables et constantes nécessaires à l’espace privé et
 * utiles au plugin sms_avec_listes
 *
 * @plugin     sms_avec_listes
 * @copyright  2022
 * @author     Vincent CALLIES
 * @licence    MIT license
 * @package    SPIP\Sms_avec_listes\Options
 */

if (!defined('_ECRIRE_INC_VERSION')) return;


defined('_DELAIS_DU_JOB_ENVOYER_SMS') || define('_DELAIS_DU_JOB_ENVOYER_SMS', ['+ 5 minutes', '+ 10 minutes', '+ 20 minutes', '+ 30 minutes', '+ 1 hour', '+ 2 hours']);