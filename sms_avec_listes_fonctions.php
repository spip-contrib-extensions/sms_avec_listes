<?php
/**
 * Fonctions utiles au plugin sms_avec_listes
 *
 * @plugin     sms_avec_listes
 * @copyright  2022
 * @author     Vincent CALLIES
 * @licence    MIT license
 * @package    SPIP\Sms_avec_listes\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

/**
 * Filtre normalisant un numéro de téléphone
 *
 * si le numéro n'est pas indentifiable, le filtre le retourne sans normalisation
 *
 * ```
 *     [(#NUMERO|normaliser_numero{#ARRAY{pays,fr,normalisation_esthetique,oui}})]
 *     [(#NUMERO|sms_liste_normaliser_numero{#ARRAY{pays,fr,retour_booleen,oui}}|non)anomalie]
 * ```
 */
function sms_liste_normaliser_numero($tel, $options_enventuelles = ['pays' => 'fr']){
	$options_enventuelles['normaliser'] = 'oui';
	if ($tel) {
		// il faut vérifier la validité des numéros transmis
		$verifier = charger_fonction('verifier', 'inc/');
		$type_de_test = 'numero_e164';
		if ($erreur = $verifier($tel, $type_de_test, $options_enventuelles, $normalise)){
			if (isset($options_enventuelles['retour_booleen']) and $options_enventuelles['retour_booleen'] === 'oui'){
				return false;
			} else {
				return $tel;
			}
		}
		$tel = $normalise;
	} elseif (isset($options_enventuelles['retour_booleen']) and $options_enventuelles['retour_booleen'] === 'oui'){
		return false;
	}
	if (isset($options_enventuelles['retour_booleen']) and $options_enventuelles['retour_booleen'] === 'oui'){
		return true;
	} else {
		return $tel;
	}
}

/**
 * Fonction utilisée en filtre pour obtenir des informations sur la liste du prestataire identifiée par son id.
 *
 * si une seule information est demandée, elle est retrounée sous la forme d'une chaine de caractère (string)
 * si plusieurs information sont demandées, elles sont retournées sous la forme d'un trableau (array).
 *
 * @uses lister_sms()
 *
 * @param string id identifiant unique de la liste du pretataire
 * @param string args arguments indiquants les informations souhaitées, par défaut le nom de la liste
 *
 * @return array|string
 */
function infos_sms_liste($id, $args = 'name'){
	if ($args = (string) $args){
		// plusieurs arguments peuvent être indiqués à la suite séparés par un espace
		$args = explode(' ', $args);
		$infos = [];
		if ($id = (string) $id and $lister_sms = charger_fonction('lister_sms', 'inc')){
			$instruction = 'annuaire';
			$retour = $lister_sms($instruction);
			if ($retour['message'] == 'OK' and isset($retour['lists']) and is_array($retour['lists'])){
				foreach ($retour['lists'] as $key => $value) {
					if (isset($value['id']) and $value['id'] == $id){
						if ($args[0]=='*') {
							$infos = $value;
						} else {
							foreach ($args as $_arg) {
								if ( in_array($_arg,$args) and isset($value[$_arg]) ) {
									$infos[$_arg] = $value[$_arg];
								}
							}
						}
					}
				}
				if ($infos){
					if (count($infos) == 1){
						return $infos[$args[0]];
					}
					return $infos;
				}
			}
		}
	}
	return '';
}

/**
 * Fonction retournant l'annuaire des listes,
 * ou des informations sur la liste ou les listes du prestataire à partir d'une identification par le nom.
 *
 * @uses lister_sms()
 *
 * @param string, par defaut '*' pour retourner l'ensemble de l'annuaire. Sinon, recherche sur un nom de la liste.
 * @param string args arguments indiquants les informations souhaitées, par défaut l'id de la liste
 *
 * @return array|string
 * Attention, les noms ne sont pas des identifiants uniques et le retour peut être au pluriel :
 * -- si une seule information est obtenue, elle est retrounée sous la forme d'une chaine de caractère (string)
 * -- si plusieurs information sont obtenues, elles sont retournées sous la forme d'un tableau (array).
 */
function sms_annuaire_des_listes($name = '*', $args = 'id'){
	if ($name = (string) $name){
		// plusieurs arguments peuvent être indiqués à la suite séparés par un espace
		$args = explode(' ', $args);
		$instruction = 'annuaire';
		$infos = [];$i=0;
		$lister_sms = charger_fonction('lister_sms', 'inc');
		$retour = $lister_sms($instruction);
		if (isset($retour['message']) and $retour['message'] == 'OK' and isset($retour['lists']) and is_array($retour['lists'])){
			if ($name === '*'){
				return $retour['lists'];
			} else {
				foreach ($retour['lists'] as $key => $value) {
					// si un annuaire est précisé par son nom
					if (isset($value['name']) and $value['name'] === $name){
					// et que l'on veut toutes les infos
					if ($args[0]=='*') {
							$infos[] = $value;
						} else {
						// ou que l'on veut certaines infos
							foreach ($args as $_arg) {
								if ( in_array($_arg,$args) and isset($value[$_arg]) ) {
									$infos[$i][$_arg] = $value[$_arg];
								}
							}
							$i++;
						}
					}
				}
			}
			if ($infos){
				// si on a qu'une seule information, la retourner sous la forme d'une chaine de caractère (string)
				if (count($infos) == 1 and count($infos[0]) == 1 and is_string($infos[0][$_arg])){
					return $infos[0][$_arg];
				}
				// sinon retourner le tableau (array)
				return $infos;
			}
		}
	}
	return '';
}