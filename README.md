Le plugin SMS_avec_liste

# Spécificités techniques

Le plugin est dépendant (nécessite) : 
	- `SMS`
mais aussi
	- `Saisies pour formulaire` 
	- `Vérifier`
	- `Yaml`
Le plugin utilise :
	- `Coordonnees`

Nécessite SPIP > 4.0.0

# Une fonction centrale pour manipuler les listes chez le prestataire

Créer, modifier, supprimer des listes et leurs contacts chez le prestataire avec une nouvelle fonction `lister_sms`
	- la fonction suit les mêmes principes que la fonction `envoyer_sms`, à savoir qu'elle permet la gestion de plusieurs prestataires par l'appel de sous fonctions (pour l'instant un seul prestataire est défini : `SMSFactor`, avec la fonction `smsfactor_liste`)
	- le resultat de la fonction est un tableau. Ce résultat est nécessairement non booléen (car le retour doit pouvoir permettre de retourner des informations en GET envoyé à l'API, par exemple pour pouvoir connaitre les contacts associés à la liste ou la liste des listes).

# Des objets éditoriaux virtuels créant un environnement familier pour gérer les listes chez le prestataire

Une interface avec les services du prestataire est créée sous forme d'objets éditoriaux "virtuels".

L'ensemble des listes est directement accessibles par le Menu `Edition` | `Annuaire des listes`

Des objets éditoriaux SPIP facilitent le pilotage de l'API distante. Un **annuaire** permet d'obtenir la liste des listes du prestataire : `../prive/squelettes/contenu/annuaire_des_listes.html`, `../prive/squelettes/inclure/annuaire_des_listes.html`.  Les **listes** du prestataire et ses **contacts** visiblent comme un objet SPIP ordinaire : `../prive/squelettes/contenu/sms_liste.html`, `../prive/squelettes/inclure/sms_liste.html`, `../prive/squelettes/hierarchie/sms_liste.html`.

Des formulaires permettent l'édition de ces objets virtuels. L'édition de listes : `../formulaires/editer_sms_liste.php & html`. L'édition des contacts : `../formulaires/editer_sms_contact.php & html`.

Des actions simplifient l'appel de l'API distante du prestataire. Il est ainsi possible de supprimer un contact : `../action/supprimer_sms_contact.php`, ou de supprimer une liste : `../action/supprimer_sms_liste.php`.

# Un formulaire générique pour contrôler l'envoi de SMS (normaux ou via une liste)

L'**envoi de SMS** n'est pas anodin et génère une responsabilité. Un formulaire est proposé pour maitriser ce traitement : `../formulaires/envoyer_sms.php & html`.

Ce formulaire est sous le contrôle de deux autorisations appelées par `../prive/squelettes/contenu/sms_envoi.html`, pour l'envoi de SMS : `[(#AUTORISER{envoyer,_sms,#ENV{id}}|sinon_interdire_acces)]`, pour l'envoi de SMS au travers d'une liste hébergée chez le prestataire : `[(#AUTORISER{sms_envoyerdans,_sms_liste,#ENV{id}}|sinon_interdire_acces)]`

# La modalité d'envoi d'un SMS peut être définie comme immédiate ou par tâche

L'envoi de SMS peut être **immédiat** ou bien par **tâche CRON**. Le plugin permet de définir cette modalité par le Menu `Configuration` | `Configuration SMS_avec_listes`. Par défaut, l'envoi sera immédiat.

# Une plage horaire définissable et contrôlable

La définition d'une **plage horaire** est centrale pour maitriser l'envoi de SMS. Il s'agit de répondre à la question "Quand un SMS peut-être envoyé ?" afin de ne pas importuner les usagers mais aussi de respecter d'éventuelles restrictions légales.

Le plugin permet de définir cette plage horaire par le Menu `Configuration` | `Configuration SMS_avec_listes`

Une autorisation permet d'avoir un feu vert (true) ou rouge (false). Un exemple d'utilisation est donné lors de l'affichage d'une boite d'alerte lors de l'utilisation du formulaire : `formulaires/envoyer_sms.php & html`, `../prive/squelettes/navigation/sms_envoi.html`. Il est possible d'être plus autoritaire et de bloquer tout usage faisant dépendre les autorisations relatives à l'envoi d'SMS ou l'envoi d'SMS à une liste de cette autorisation : `../sms_avec_listes_autorisations.php`.

Une fonction est à disposition pour renvoyer, sans prendre en considération la qualité de l'auteur, si la période est bonne ou pas : `../inc/plage_horaire_sans_sms.php`. Son appel se fait de la façon suivante : `$plage_horaire = charger_fonction('plage_horaire_sans_sms', 'inc');` `if ($plage_horaire()){ echo 'on ne devrait pas envoyer'; } else { echo 'on peut envoyer'; }`
 

# Une boite à outils de maintenance pour vérifier la qualité des données et permettre l'export chez le prestataire

Spécialement dédiée à une utilisation simultanée du plugin Coordonnées, une boite à outils de maintenance, pour les développeurs, est proposée dans le repertoire `../sms_listes`. On y trouve :

- une fonction `objets_portables` qui retourne la liste des objets auxquels le plugin coordonnées a associés des numéros de téléphones portables.

- des fichiers Yaml décrivant le schéma à suivre pour exporter des numéros de téléphones portables vers le prestaire et créer une liste.

- une fonction `export_objets` pour mettre en oeuvre les schémas et obtenir des tableaux formatés pour la fonction `lister_sms` qui fait les requetes auprès du presataire.

Ces outils sont directement utilisables au travers d'un panneau accessible par le Menu `Maintenance` | `Maintenance des listes`

Ce panneau affiche successivement :

1°) Des onglets, qui permettent de voir les objets auxquels des numéros de téléphones portables sont associés par le plugin Cooordonnées. Cela permet d'avoir un inventaire des types de numéros détenus sur le site SPIP.

2°) Une liste numéros. Cela permet leur normalisation. Le contrôle de la qualité de la donnée avant son transfert est important : les numéros non conforme ou les doublons peuvent créer des surcouts inutiles.
  voir : `../verifier/numero_e164.php`
         `../action/sms_liste_normaliser_numero.php`
         `../action/sms_liste_normaliser_tout.php`

3°) Les listes créées à partir de ces objets chez le prestataire. Cela permet de vérifier si une mise à jour est nécessaire (notamment en regardant d'un coup d'oeil si une liste à plus de numéros que l'autre.

4°) Le schéma éventuel déclaré (par un fichier Yaml) d'export et sa requete SQL. Ces données techniques permettent de s'assurer que l'opération de transfert des données du site SPIP vers le site du prestataire est bien celle que l'on souhaite faire. Un exemple est donnée pour l'objet `auteur` 
  voir : `../sms_listes/auteurs.yaml`

5°) Un bouton sécurisé qui permet d'un clic la création d'une liste chez le prestataire. Un exemple est donnée pour l'objet `auteur` 
  voir : `../prive/squelettes/inclure/sms_liste_auteurs.html` et l'action `../action/sms_liste_objets.php`

# Un principe de minimisation des données

Aucune information sur les listes ou les contacts n'est conservée sur le site hébergeant SPIP. 
o La création de liste, l'attribution d'un numéro unique à celles-ci, les numéros confiés sont exclusivement dans la base de données du prestataire. L'objectif est de suivre le principe RGPD de minimisation des données collectées et d'éviter une redondance de la même information sous différentes formes sur le site hébergeant SPIP et sur le site du prestataire. En d'autres termes, le sous-traitant à ses données propres dont il est responsable.

# Futur
1°) L'intégration du DSK de SMSFactor pourrait être intéressant. L'appel des (multiples) classes qu'il offre pourrait se faire à partir de la sous-fonction de `lister_sms`, `smsfactor_liste`.
2°) Il faut développer la fonction pour octopush
3°) pour l'instant la vérification des numéros de téléphones n'est qu'au format français. Il faudrait permettre une internationalisation.
4°) intenationalisation du plugin (idome de langue)
5°) prévoir l'inclusion de timezone dans la gestion de la plage horaire
