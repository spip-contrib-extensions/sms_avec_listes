<?php
/**
 * L'action supprimer une liste
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Action pour supprimer une liste
 *
 * Vérifier l'autorisation avant d'appeler l'action.
 *
 * @param null|int $arg
 *     Identifiant à supprimer. - Token précisé
 *     En absence de id utilise l'argument de l'action sécurisée.
**/
function action_supprimer_sms_liste_dist($arg=null) {
	if (is_null($arg)){
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
	}
	// pour supprimer une liste, il faut son identifiant unique $id
	// et un $token
	$arg = explode("-", $arg);
	[$id, $token]  = $arg;
	$options = [];

	// si pas de token transmis, on vérifier que la config en a bien un
	if ($token) {
		$options['token'] = $token;
	} else {
		include_spip('inc/config');
		if (!$options['token'] = lire_config('sms/token_smsfactor')){
			spip_log('lister_sms - ' . _T('sms_liste:err_liste_token'), 'sms_liste.' . _LOG_INFO);
			exit;
		}
	}

	if (isset($id) and $id){
		$lister_sms = charger_fonction('lister_sms', 'inc');
		$instruction = 'supprimer_liste';
		$retour = $lister_sms($instruction, [$id], $options);
		if ($retour['message'] != 'OK'){
			spip_log('lister_sms - ' . print_r($retour,true), 'sms_liste.' . _LOG_ERREUR);
		}
	} else {
			spip_log('lister_sms - ' . _T('sms_liste:err_liste_id',['instruction' => $instruction]), 'sms_liste.' . _LOG_INFO);
	}
}
