<?php
/**
 * L'action supprimer un contact
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Action pour supprimer un contact
 *
 * Vérifier l'autorisation avant d'appeler l'action.
 *
 * @param null|int $arg
 *     Identifiant à supprimer.
 *     En absence de id utilise l'argument de l'action sécurisée.
**/
function action_supprimer_sms_contact_dist($arg=null) {
	if (is_null($arg)){
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
	}
	// pour supprimer un contact, il faut son identifiant unique
	// et s'assurer que l'on a bien un token
	$arg = explode("-", $arg);
	$instruction = 'supprimer_contact';
	$options = [];
	list($id, $token) = $arg;
	// si pas de token transmis, on vérifier que la config en a bien un
	if ($token) {
		$options['token'] = $token;
	} else {
		include_spip('inc/config');
		if (!lire_config('sms/token_smsfactor')){
			spip_log('lister_sms - ' . _T('sms_liste:err_contact_token'), 'sms_liste.' . _LOG_INFO);
			exit;
		}
	}

	if ($id){
		$lister_sms = charger_fonction('lister_sms', 'inc');
		$retour = $lister_sms($instruction, [$id], $options);
		if ($retour['message'] != 'OK'){
			spip_log('lister_sms - ' . print_r($retour,true), 'sms_liste.' . _LOG_ERREUR);
		}
	} else {
			spip_log('lister_sms - ' . _T('sms_liste:err_contact_id',['instruction' => $instruction]), 'sms_liste.' . _LOG_INFO);
	}
}
