<?php
/**
 * L'action pour exporter les données des contacts sur un objet et créer une liste chez le prestataire
 *
 * Un schéma d'export des données est proposé ../sms_listes/OBJETs.yaml
 * Quelques variables de langues : ../lang/sms_liste_fr.php
 * Une noisette pour appeler la présente action de peuplement : ../prive/squelettes/inclure/sms_liste_OBJETs.html
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Action pour la création d'une liste chez le prestataire avec les informations des auteurs
 *
 * Vérifier l'autorisation avant d'appeler l'action.
 *
 * @param null|int $arg
 *     Identifiant à supprimer.
 *     En absence de id utilise l'argument de l'action sécurisée.
 */
function action_sms_liste_objets_dist($arg=null) {
	if (is_null($arg)){
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
	}

	// pour créer une liste, il faut au moins un $objets et un $token
	$arg = explode("-", $arg);
	[$objets, $token]  = $arg;
	$options = [];
	// si pas de token transmis, on vérifier que la config en a bien un
	if (isset($token) and !empty($token)) {
		$options['token'] = $token;
	} else {
		include_spip('inc/config');
		if (!$options['token'] = lire_config('sms/token_smsfactor')){
			spip_log('lister_sms - ' . _T('sms_liste:err_contact_token'), 'sms_liste.' . _LOG_ERREUR);
			exit;
		}
	}
	// le nom de la liste sera $objets
	$options['name'] = $objets;
	// avoir le schéma d'export (fichier yaml) et les données exportées selon le schéma
	if ($objets){
		if ( $yaml = find_in_path("sms_listes/$objets.yaml")
			and include_spip('inc/yaml')
			and $schema = yaml_decode_file( $yaml, array('inclure' => true) )
			and $peuple = charger_fonction( 'export_objets', 'sms_listes' )
			and $options['contacts'] = $peuple($schema)
		){
			$options['contacts'] = $peuple($schema);
			// charger la fonction lister_sms() et créer la liste
			if ( $lister_sms = charger_fonction('lister_sms', 'inc') 
			){
				$instruction = 'creer';
				$retour = $lister_sms($instruction, [], $options);
				// s'il y a une erreur, la placer dans le log
				if ($retour['message'] != 'OK')
				{
					spip_log('lister_sms - ' . print_r($retour, true), 'sms_liste.' . _LOG_ERREUR);
				}
			} 
		}
	}
}