<?php
/**
 * L'action normaliser tous les numéros
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Action pour normaliser l'ensemble des numéros
 *
 * Vérifier l'autorisation avant d'appeler l'action.
 * 
 * @uses API verifier
 * @uses API editer_objet
 * @param null|int $objet
 *     objet éventuel pour limiter la normalisation des numéros à ceux qui lui sont associés
 *     En absence de objet utilise l'argument de l'action sécurisée.
**/
function action_sms_liste_normaliser_tout_dist($objet=null) {
	if (is_null($objet)){
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$objet = $securiser_action();
	}
	include_spip('base/objets');
	if ($objet){
		$objets = [table_objet_sql($objet)];
	} else {
		$objets_portables = charger_fonction('objets_portables', 'sms_listes');
		$objets =  $objets_portables([]);
		$objets = array_keys($objets);
	}
	include_spip('action/editer_objet');
	foreach ($objets as $key => $table) {
		$objet = objet_type($table);
		// Connaitre les numeros de téléphones portables qui sont associés à l'objet
		// -- l'objet est dans une table de liens qui contient l'identifiant unique du numéro et aussi son type.
		//    Le type pour un téléphone portable est ’cell’.
		// -- le numéros de téléphone est dans la tables spip_numeros auquel on accède par l'identifiant unique,
		// Il est donc nécessaire de faire une jointure des deux table avec l'identifiant unique.
		$select = ['numeros.numero','numeros.id_numero'];
		$from = ['spip_numeros_liens as liens', 'spip_numeros as numeros'];
		$where = [
			'liens.objet=' . sql_quote($objet),
			'liens.id_numero=numeros.id_numero',
			'liens.type=' . sql_quote('cell'),
		];
		if ($liens = sql_allfetsel($select, $from, $where)) {
			foreach ($liens as $l) {
				// il faut vérifier la validité et normaliser
				$verifier = charger_fonction('verifier', 'inc/');
				$type_de_test = 'numero_e164';
				$options_enventuelles = ['pays' => 'fr','normaliser' => 'oui'];
				$numero_normalise = '';
				if ($erreur = $verifier($l['numero'], $type_de_test, $options_enventuelles, $numero_normalise)){
					// permettre de retrouver dans le log les numéros que ne peuvent être normalisés
					spip_log('sms_liste_normaliser_tout ' . $erreur . ' id_numero=' . $l['id_numero'] . ' ('. $l['numero'] .')', 'sms_liste.' . _LOG_DEBUG);
				}
				// s'il y a eu normalisation, il faut faire la modification
				if ($numero_normalise and $l['numero'] != $numero_normalise){
					$id_objet = objet_modifier('numero', $l['id_numero'], ['numero' => $numero_normalise]);
				}
			}
		}
	}
}
