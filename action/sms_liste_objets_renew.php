<?php
/**
 * L'action pour rafraichir une liste chez le prestataire
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Action pour rafraichir la liste chez le prestataire avec les informations d'un objet
 *
 * Vérifier l'autorisation avant d'appeler l'action.
 * L'action va :
 * -- supprimer la liste existante
 * -- recréer une liste sur la base des numéros nouvellement collectés.
 *
 * @uses find_in_path, yaml_decode_file pour retrouver et décoder le schéma de peuplement
 * @uses charger_fonction, lister_sms pour supprimer et créer la liste
 *
 * @param null|string $arg
 *     - objet
 *     - id à supprimer
 *     - token
 *     En absence de l'argument transmis, utilise l'argument de l'action sécurisée.
 * @return void
 */
function action_sms_liste_objets_renew_dist($arg=null) {
	if (is_null($arg)){
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
	}

	// pour créer une liste, il faut au moins un $objets et un $token
	$arg = explode("-", $arg);
	[$objets, $id, $token] = $arg;
	$options = [];
	// si pas de token transmis, on vérifier que la config en a bien un
	if (isset($token) and !empty($token)) {
		$options['token'] = $token;
	} else {
		include_spip('inc/config');
		if (!$options['token'] = lire_config('sms/token_smsfactor')){
			spip_log('lister_sms - ' . _T('sms_liste:err_contact_token'), 'sms_liste.' . _LOG_ERREUR);
			exit;
		}
	}

	if ($lister_sms = charger_fonction('lister_sms', 'inc')){

		// on supprime la liste
		if (isset($id) and $id){
			$instruction = 'supprimer_liste';
			$retour = $lister_sms($instruction, [$id], $options);
			if ($retour['message'] != 'OK'){
				spip_log('lister_sms - ' . print_r($retour,true), 'sms_liste.' . _LOG_ERREUR);
			}
		} else {
			spip_log('lister_sms - ' . _T('sms_liste:err_liste_id',['instruction' => $instruction]), 'sms_liste.' . _LOG_INFO);
			exit; // si on ne peut pas supprimer, on s'arrête
		}
	
		// on (re)crée la liste
		if ($options['name'] = $objets){
			// on charge et décode le descriptif, on l'interprète et charge les données de peuplement
			if ( $yaml = find_in_path("sms_listes/$objets.yaml")
				and include_spip('inc/yaml')
				and $schema = yaml_decode_file( $yaml, array('inclure' => true) )
				and $peuple = charger_fonction( 'export_objets', 'sms_listes' )
				and $options['contacts'] = $peuple($schema)
			){
				$options['contacts'] = $peuple($schema);
				// créer la liste
				$instruction = 'creer';
				$retour = $lister_sms($instruction, [], $options);
				// s'il y a une erreur, la placer dans le log
				if ($retour['message'] != 'OK')
				{
					spip_log('lister_sms - ' . print_r($retour, true), 'sms_liste.' . _LOG_ERREUR);
				} else {
					// si la liste a été créée avec succès, il faut rediriger dessus !
					include_spip('inc/headers');
					$redirect = generer_url_ecrire('sms_liste','id='.$retour['id']);
					// remplace le &amp; par un vrai &. L'encodage de l'esperluette n'est pas aimé par la fonction redirige_par_entete()
					$redirect = str_replace('&amp;','&',$redirect);
					redirige_par_entete($redirect);
				}
			}
		}
	}
}