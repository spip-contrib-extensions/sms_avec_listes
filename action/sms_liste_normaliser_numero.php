<?php
/**
 * L'action normaliser un numéro
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Action pour normaliser un numéro
 *
 * Vérifier l'autorisation avant d'appeler l'action.
 * 
 * @uses API verifier
 * @uses API editer_objet
 * @param null|int $arg
 *     Identifiant à supprimer. - Token précisé
 *     En absence de id utilise l'argument de l'action sécurisée.
**/
function action_sms_liste_normaliser_numero_dist($id_numero=null) {
	if (is_null($id_numero)){
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$id_numero = $securiser_action();
	}
	if ($id_numero = intval($id_numero)){
		// il faut avoir le numéro
		if ($numero = sql_getfetsel('numero', 'spip_numeros', "id_numero=$id_numero")){
			// il faut vérifier la validité et normaliser
			$verifier = charger_fonction('verifier', 'inc/');
			$type_de_test = 'numero_e164';
			$options_enventuelles = ['pays' => 'fr','normaliser' => 'oui'];
			$numero_normalise = '';
			if ($erreur = $verifier($numero, $type_de_test, $options_enventuelles, $numero_normalise)){
				$valeurs['message_erreur'] = $erreur;
				$valeurs['editable'] = false;
			}
			// s'il y a eu normalisation, il faut faire la modification
			if ($numero != $numero_normalise){
				include_spip('action/editer_objet');
				$id_objet = objet_modifier('numero', $id_numero, ['numero' => $numero_normalise]);
			}
		}
	}
}
