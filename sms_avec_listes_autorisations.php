<?php
/**
 * Fonctions définissant les autorisations de faire ou de ne pas faire des actions
 *
 * @plugin     sms_avec_listes
 * @copyright  2022
 * @author     Vincent CALLIES
 * @licence     MIT license
 * @package    SPIP\Sms_avec_listes\Fonctions
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) return;

/**
 * Fonction appelée par le pipeline
 *
 */
function sms_avec_listes_autoriser(){}

/**
 * Autorisation d'envoyer un SMS via le site du prestataire
 *
 * @param string          $faire
 *      L'action se nomme ’envoyer’
 * @param string|null     $type
 *      Le type est toujours ’sms’.
 * @param string|int|null $id
 *      Identifiant unique. 
 *         Représente celui de la liste si cela est précisé dans $options
 * @param null|int|array  $qui
 *      Description de l'auteur demandant l'autorisation
 * @param array           $opt
 *      Options de cette autorisation
 *
 * @return bool
 *      true s'il a le droit, false sinon
 */
function autoriser_sms_envoyer_dist($faire, $type, $id, $qui, $opt) {
	// une autorisation dépendant des horaires peut être faite :
	// return autoriser('sms_envoyerdans', '_plage_decente', $id, $qui, $opt);
	return in_array($qui['statut'], array('0minirezo','1comite'));
}

/**
 * Autorisation d'envoyer un SMS via le site du prestataire dans une plage horaire décente
 *
 * @uses plage_horaire_sans_sms()
 *       Il est possible d'utiliser le plugin Timezone qui vous permet de définir la timezone du site
 *
 * @param string          $faire
 *      L'action se nomme ’sms_envoyerdans’
 * @param string|null     $type
 *      Le type est toujours ’_plage_horaire’.
 * @param string|int|null $id
 *      Identifiant unique. 
 *         Représente celui de la liste si cela est précisé dans $options
 * @param null|int|array  $qui
 *      Description de l'auteur demandant l'autorisation
 * @param array           $opt
 *      Options de cette autorisation
 *
 * @return bool
 *      true s'il a le droit, false sinon
 */
function autoriser_plagehoraire_sms_envoyerdans_dist($faire, $type, $id, $qui, $opt) {
	$plage_horaire = charger_fonction('plage_horaire_sans_sms', 'inc');
	if ($plage_horaire()){ 
		// la fonction renvoie ’true’, on est
		// dans la plage des heures et jours interdits.
		// on ne peut pas envoyer de SMS.
		return false;
	} else {
		// la fonction renvoie ’false’, on n'est pas
		// dans la plage des heures et jours interdits.
		// on peut envoyer un SMS.
		return in_array($qui['statut'], array('0minirezo','1comite'));
	}
	return false;
}

/**
 * Autorisation d'envoyer un SMS dans une liste via le site du prestataire
 *
 * @param string          $faire
 *      L'action se nomme ’sms_envoyerdans’
 * @param string|null     $type
 *      Le type est toujours ’sms_liste’.
 * @param string|int|null $id
 *      Identifiant unique. 
 *         Représente celui de la liste si cela est précisé dans $options
 * @param null|int|array  $qui
 *      Description de l'auteur demandant l'autorisation
 * @param array           $opt
 *      Options de cette autorisation
 *
 * @return bool
 *      true s'il a le droit, false sinon
 */
function autoriser_smsliste_sms_envoyerdans_dist($faire, $type, $id, $qui, $opt) {
	// l'envoi d'un SMS à une liste est une administration avancée
	return $qui['statut'] == '0minirezo' OR $qui['webmestre'] == 'oui';
}

/**
 * Autorisation de voir l'item du menu ’liste’ dans le menu ’Edition’
 *
 * À noter que le nom du type (annuaire_des_listes) repris dans le nom de la fonction d'autorisation, 
 * retire les underscores. Ce n'est pas une anomalie, toutes les fonctions d'autorisation suivent cette normalisation.
 *
 * @param string          $faire
 *      L'action se nomme ’menu’
 * @param string|null     $type
 *      Le type est ’annuaire_des_listes’.
 *      Comme ce n'est pas un objet, lors de l'appel faire précéder le type par un underscore.
 *      ``` #AUTORISER{creer,_sms_liste} ```
 * @param string|int|null $id
 *      inutilisé
 * @param null|int|array  $qui
 *      Description de l'auteur demandant l'autorisation
 * @param array           $opt
 *      inutilisé
 *
 * @return bool
 *      true s'il a le droit, false sinon
 */
function autoriser_annuairedeslistes_menu_dist($faire, $type, $id, $qui, $opt) {
	return in_array($qui['statut'], ['0minirezo','1comite']);
}

/**
 * Autorisation de voir une liste sur le site du prestataire
 *
 * À noter que le nom du type (sms_liste) repris dans le nom de la fonction d'autorisation, 
 * retire l'underscore. Ce n'est pas une anomalie, toutes les fonctions d'autorisation suivent cette normalisation.
 *
 * @param string          $faire
 *      L'action se nomme ’voir’
 * @param string|null     $type
 *      Le type est toujours ’sms_liste’.
 *      Comme ce n'est pas un objet, lors de l'appel faire précéder le type par un underscore.
 *      ``` #AUTORISER{voir,_sms_liste} ```
 * @param string|int|null $id
 *      En création l'$id sera ’new’ ou ’oui’ ou inutilisé
 * @param null|int|array  $qui
 *      Description de l'auteur demandant l'autorisation
 * @param array           $opt
 *      Options de cette autorisation
 *
 * @return bool
 *      true s'il a le droit, false sinon
 */
function autoriser_smsliste_voir_dist($faire, $type, $id, $qui, $opt) {
	return in_array($qui['statut'], ['0minirezo','1comite']);
}

/**
 * Autorisation de créer une liste sur le site du prestataire
 *
 * À noter que le nom du type (sms_liste) repris dans le nom de la fonction d'autorisation, 
 * retire l'underscore. Ce n'est pas une anomalie, toutes les fonctions d'autorisation suivent cette normalisation.
 *
 * @param string          $faire
 *      L'action se nomme ’creer’
 * @param string|null     $type
 *      Le type est toujours ’sms_liste’.
 *      Comme ce n'est pas un objet, lors de l'appel faire précéder le type par un underscore.
 *      ``` #AUTORISER{creer,_sms_liste} ```
 * @param string|int|null $id
 *      En création l'$id sera ’new’ ou ’oui’ ou inutilisé
 * @param null|int|array  $qui
 *      Description de l'auteur demandant l'autorisation
 * @param array           $opt
 *      Options de cette autorisation
 *
 * @return bool
 *      true s'il a le droit, false sinon
 */
function autoriser_smsliste_creer_dist($faire, $type, $id, $qui, $opt) {
	// créer une liste sur le site du presataire est une administration avancée
	return $qui['statut'] == '0minirezo' OR $qui['webmestre'] == 'oui';
}

/**
 * Autorisation de supprimer une liste sur le site du prestataire
 *
 * À noter que le nom du type (sms_liste) repris dans le nom de la fonction d'autorisation, 
 * retire l'underscore. Ce n'est pas une anomalie, toutes les fonctions d'autorisation suivent cette normalisation.
 *
 * @param string          $faire
 *      L'action se nomme ’supprimer’
 * @param string|null     $type
 *      Le type est toujours ’sms_liste’.
 *      Comme ce n'est pas un objet, lors de l'appel faire précéder le type par un underscore.
 *      ``` #AUTORISER{supprimer,_sms_liste} ```
 * @param string|int|null $id
 *      En création l'$id sera ’new’ ou ’oui’ ou inutilisé
 * @param null|int|array  $qui
 *      Description de l'auteur demandant l'autorisation
 * @param array           $opt
 *      Options de cette autorisation
 *
 * @return bool
 *      true s'il a le droit, false sinon
 */
function autoriser_smsliste_supprimer_dist($faire, $type, $id, $qui, $opt) {
	// Supprimer une liste sur le site du presataire est une administration avancée
	return $qui['statut'] == '0minirezo' OR $qui['webmestre'] == 'oui';
}

/**
 * Autorisation de modifier une liste sur le site du prestataire
 *
 * À noter que le nom du type (sms_liste) repris dans le nom de la fonction d'autorisation, 
 * retire l'underscore. Ce n'est pas une anomalie, toutes les fonctions d'autorisation suivent cette normalisation.
 *
 * @param string          $faire
 *      L'action se nomme ’modifier’
 * @param string|null     $type
 *      Le type est toujours ’sms_liste’.
 *      Comme ce n'est pas un objet, lors de l'appel faire précéder le type par un underscore.
 *      ``` #AUTORISER{modifier,_sms_liste} ```
 * @param string|int|null $id
 *      En modification l'$id correspondra à l'identifiant unique donnée par le prestataire
 * @param null|int|array  $qui
 *      Description de l'auteur demandant l'autorisation
 * @param array           $opt
 *      Options de cette autorisation
 *
 * @return bool
 *      true s'il a le droit, false sinon
 */
function autoriser_smsliste_modifier_dist($faire, $type, $id, $qui, $opt) {
	// modifier une liste sur le site du presataire est une administration avancée
	// que SMSFactor ne semble pas prendre en charge
	return false;
}

/**
 * Autorisation de créer un contact sur le site du prestataire
 *
 * À noter que le nom du type (sms_contact) repris dans le nom de la fonction d'autorisation, 
 * retire l'underscore. Ce n'est pas une anomalie, toutes les fonctions d'autorisation suivent cette normalisation.
 *
 * @param string          $faire
 *      L'action se nomme ’creer’
 * @param string|null     $type
 *      Le type est toujours ’sms_contact’.
 *      Comme ce n'est pas un objet, lors de l'appel faire précéder le type par un underscore.
 *      ``` #AUTORISER{creer,_sms_contact} ```
 * @param string|int|null $id
 *      En création l'$id sera ’new’ ou ’oui’ ou inutilisé
 * @param null|int|array  $qui
 *      Description de l'auteur demandant l'autorisation
 * @param array           $opt
 *      Options de cette autorisation
 *
 * @return bool
 *      true s'il a le droit, false sinon
 */
function autoriser_smscontact_creer_dist($faire, $type, $id, $qui, $opt) {
	// créer un contact sur le site du presataire est une administration avancée
	return $qui['statut'] == '0minirezo' OR $qui['webmestre'] == 'oui';
}

/**
 * Autorisation de créer un contact dans une liste via le site du prestataire
 *
 * @param string          $faire
 *      L'action se nomme ’sms_contactdans’
 * @param string|null     $type
 *      Le type est toujours ’sms_liste’.
 * @param string|int|null $id
 *      Identifiant unique. 
 *         Représente celui de la liste si cela est précisé dans $options
 * @param null|int|array  $qui
 *      Description de l'auteur demandant l'autorisation
 * @param array           $opt
 *      Options de cette autorisation
 *
 * @return bool
 *      true s'il a le droit, false sinon
 */
function autoriser_smsliste_creersms_contactdans_dist($faire, $type, $id, $qui, $opt) {
	// l'envoi d'un SMS à une liste est une administration avancée
	return $qui['statut'] == '0minirezo' OR $qui['webmestre'] == 'oui';
}

