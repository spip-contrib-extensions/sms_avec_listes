<?php
/**
 * Utilisations de pipelines par Sms_avec_listes
 *
 * @plugin     sms_avec_listes
 * @copyright  2022
 * @author     Vincent CALLIES
 * @licence    MIT license
 * @package    SPIP\Sms_avec_listes\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Utiliser ce pipeline permet d'ajouter un contexte dans la colonne de gauche (infos ou navigation).
 *
 * @pipeline affiche_gauche
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function sms_avec_listes_affiche_gauche($flux){

	// on a un exec
	if (isset($flux['args']['exec']) and $exec = $flux['args']['exec'])
	{ 
		$texte ='';
		// un SMS sera envoyé à une liste du prestataire, on veut avoir la liste des champs de fusion.
		if ($exec === 'sms_envoi' and _request('type') === 'liste' and isset($flux['args']['id']) and !empty($flux['args']['id'])) {
			$texte .= recuperer_fond('prive/squelettes/inclure/sms_envoyer_sur_liste-infos', [
					'identifiant_liste' => $flux['args']['id'],
					'n_contact' => _request('n_contact')
				],
				['ajax'=> true], // activation de l'ajax
			);
		}

		if ($texte){
			$flux['data'] .= $texte;
		}

	}
	return $flux;

}


/**
 * Utiliser ce pipeline permet d'ajouter une aide au plugin,
 *
 * @pipeline aide_index
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function sms_avec_listes_aide_index($index) {
	$index['smsmessage'] = [
		'longueur',
		'caracteres',
		'champs',
	];
	$index['listemaintenance'] = [
		'objets',
		'numeros',
		'listes',
		'schema',
		'peuplement',
	];
	$index['smstemporalite'] = [
		'immediat',
		'differe',
	];
	$index['smsqualification'] = [
		'alerte',
		'marketing',
	];
	return $index;
}